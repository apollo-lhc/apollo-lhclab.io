#Manual firmware builds

 In cases where you want to compile the SM Firmware manually, you can follow the instructions below. The instructions detail how to compile the firmware on Tesla; hence, you need to have an account on this machine. Once you have a computer account at Tesla, you can SSH using your username:

```bash
ssh <username>@tesla.bu.edu
```
Next step is to source your vivado license. If you do not have a license Please contact [Zeynep Demiragli](https://www.bu.edu/physics/profile/zeynep-demiragli).

```bash
source /work/Xilinx/Vivado/2020.2/settings64.sh
```

Now, you are ready to check out the firmware code. Ensure that you create a feature branch for your changes, which can eventually be merged into the develop and master branches.

```bash
git clone https://gitlab.com/apollo-lhc/FW/SM_ZYNQ_FW.git OR git clone git@gitlab.com:apollo-lhc/FW/SM_ZYNQ_FW.git
git checkout develop
git checkout -b feature/name_branch
cd SM_ZYNQ_FW
make init
```

The `make init` command relies on the `Makefile`. In this file, you can find the `SUB_VARIENT_SELECTION?=.emmc` variable to determine whether you want to build emmc or sdcard images. If you plan to make any changes, you can do so now. It is important that the changes are committed to Git because the HOG framework relies on a hash of the Git commit as the basis for the build. Please note that you do not need to push yet; a simple commit will suffice to build the firmware.

```bash
./Hog/LaunchWorkflow.sh rev2a_xczu7ev -recreate | tee hog_fw_log.txt
```

Note that `rev2a_xczu7ev` corresponds to the configuration for which you want to build the firmware. If you wish to create a new configuration (e.g., rev3), you will need to create a directory under `configs` and `Top` and edit them accordingly. Be aware that there are numerous symbolic links under these folders. In your new release folder, ensure that you follow the same structure as the existing configurations.

Assuming the previous step was successful, you now need to create the address tables, overlays, and package the firmware.

```bash
make kernel/address_tables/address_table_rev2a_xczu7ev/address_apollo.xml ADDRESS_TABLE_CREATION_PATH=./kernel/
make overlays DTSI_PATH=kernel/hw/rev2a_xczu7ev/
make BUILD_PETALINUX_VERSION=2020.2 rev2a_xczu7ev
```
The end product is a tar.gz file that contains the FPGA FW, the Linux Kernel, the device tree, and the required address tables. You can find it at: zynq_build/rev2a_xczu7ev.emmc/rev2a_xczu7ev.tar.gz

