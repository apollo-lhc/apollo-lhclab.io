# Linux Kernel

Once the firmware build is completed, the Linux kernel can be built, using the source code under the `kernel` directory. Similar to the FW build, the build configuration for each SM type is defined under `configs` directory.

## Organization

  - __config_BUILD_NAME.yaml & address_tables/address_table_BUILD_NAME__  
      The config_BUILD_NAME.yaml is an output file from the FW build and contains a list of AXI endpoints, their uHAL base address, and the top uHAL XML file for it.
      The ```make kernel/address_tables/address_table_BUILD_NAME/address_apollo.xml``` rule uses this config file to build the contents of the address_tables/address_table_BUILD_NAME dir.
      The address_tables/address_table_BUILD_NAME dir contains the final address table for this FW in `address_apollo.xml` and a modules_BUILD_NAME directory containing all uHAL xml files included in the `address_apollo.xml`.  
  - __hw/BUILD_NAME__  
      This directory contains the dtsi, dtsi_post_chunk, and dtsi_chunk files use for either device tree overlays (dtsi files) or directly incorporating into the fixed device tree (post_chunk & chunk files)
  - __configs/BUILD_NAME__
     This directory contains all the modifications used when building the linux kernel for this BUILD_NAME.
  - __doc__  
      This directory contains any useful documentation for the rest of the kernel build.
  - __Makefile__
  - __scripts__
  - __zynq_build/BUILD_NAME/__
      This directory is where the build system places builds the BUILD_NAME linux kernel, uboot, and the FSBL.


#### configs/BUILD_NAME


  - __ATF__  
    These are the modifications to the Arm Trusted Firmware section of the build.
  - __configs__  
    This directory contains sub-directories for the general config and rootfs config files to use in the build.
    There are endings to general config files (.emmc & .sdcard) that are used in the build process to vary the configs for different boot modes.
  - __device-tree__
    This dir contains the script or scripts used to merge the dtsi chunk & post_chunk files into the primary device tree.
  - __fsbl__
    This dir contains the modifications to the first stage boot loader.   Primarily adding the automatic SI programming on boot.
  - __hw_user__  
      This is where extra dtsi (or chunk/post_chunk) files can be added to the build. 
  - __kernel__
      In this directory is the structure needed to modify the modules built into the kernel.
  - __kmodules__  
      This dir has the rules to build loadable kernel modules


#### Patching the Kernel

Patches to kernel configuration can be specified under the following directory: `configs/<HW>/kernel/linux`. Here, under `linux-xlnx` directory, one can find the user specified `.cfg` files that has several patches to the kernel, and all these files **must** be added to the `SRC_URI` variable on the `linux-xlnx_%.bbappend` file. For more details, please see the documentation [here](https://docs.yoctoproject.org/3.1.19/) by Yocto.


## Building

Build command is very similar to the FW build, for example for `rev2a_xczu7ev` SM:

```bash
cd kernel/
make rev2a_xczu7ev
```
