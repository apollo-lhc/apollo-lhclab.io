# YAML Config files
#### General
The YAML config files are loaded from a vivado tcl script via the `yaml_to_bd` call in the [BD library](#block-design).
Though includes are not defined in yaml, the library accepts `INCLUDE` tags that list the file to be included.
In the AXI_SLAVES portion of the yaml file, the `INCLUDE` key can(must) be used inside of a key as followes:  
```
AXI_SLAVES:
  BASE:
    INCLUDE_FILE: "${::apollo_root_path}/src/ZynqPS/config_ZynqMP.yaml"
    SUB_SLAVES:
      C2C:
        INCLUDE_FILE: "${::apollo_root_path}/src/C2C/config_c2c_usp_5g.yaml"
```
This would include the file `${::apollo_root_path}/src/ZynqPS/config_ZynqMP.yaml` inside of the `BASE` key and `${::apollo_root_path}/src/C2C/config_c2c_usp_5g.yaml` inside of the `C2C` key.
Only the [AXI_SLAVES](#axi_slaves) and [CORES](#cores) sections of the included YAML files will be processed.

[AXI_CONTROL_SETS](#axi_control_sets) can also use an include directive, but it should be at the beginning of the `AXI_CONTROL_SETS` section.
You can override an set from an include file by adding it again later.  Ex:  
```
AXI_CONTROL_SETS:
  INCLUDE_FILE:  ${::apollo_root_path}/src/CM_yaml/CM2_base.yaml
  # override default axilite endpoint                                                                                                                                                                                                                                                                                                                 
  AXILITE_ENDPOINT_CTRL:
    axi_interconnect: "${::CM_INTERCONNECT}"
    axi_clk: "${::AXI_MASTER_CLK}"
    axi_rstn: "/SYS_RESET/peripheral_aresetn"
    axi_freq: "${::AXI_MASTER_CLK_FREQ}"
    allocator:
      starting_address: 0xB3000000
      size: 0x00800000
      block_size: 4K
```

#### AXI_CONTROL_SETS
This section of the YAML file lists the AXI information used to connect up point to point AXI connections.
Ex:  
```
  AXI_MASTER_CTRL:
    axi_interconnect: "${::AXI_MAIN_INTERCONNECT}"
    axi_clk: "${::AXI_MASTER_CLK}"
    axi_rstn: "${::AXI_PRIMARY_SLAVE_RSTN}"
    axi_freq: "${::AXI_MASTER_CLK_FREQ}"
    allocator:
      starting_address: 0xA0000000
      size: 0x08000000
      block_size: 4K
```
The first key is the name to be used in the AXI_SLAVES part of the YAML file to reference this collection of connection information (in the example AXI_MASTER_CTRL).

The four following keys describe the endpoint where the AXI connection will go, in most cases an AXI interconnect, and then the clock and reset_n signals used for these master/slave ports.
Usually this is left as a variable that is specified in either the Zynq CPU build tcl, or the C2C slave interface tcl (in CM FW).
The frequency is listed because in some situations vivado doesn't know it or is unable to determine it, and specifying it here allows the automation to deal with those situations.
The frequency value is in Hz.

The `allocator` tag contains the information for the valid address ranges when connecting to this control set and how that address range is to be controlled for fixed and automatic addressing.
The range of the the memory space is `starting_address` to `starting_address + size` and the smallest allocation is `block_size`.   Requests smaller than `block_size` will be rounded up to `block_size`

#### AXI_SLAVES

The AXI_SLAVES section of the yaml file lists all the [BD library](#block-design) calls and raw tcl commands that will be run.
Each new block starts with a key which will be the default name for the operations performed in it.
The possible children for this block are:

  - TCL_CALL  
      If this key has a single value associated with it, it is the raw tcl command to run in vivado
  - SUB_SLAVES  
      This is a list of blocks to be created once the parent block has finished.  This is use to make sure a child is only created after a parent.
  - XML  
      This is the top uHAL XML file to use for this endpoint.   This will cause the automation to load it, find all other xml files it includes, and copy them all for deployment with the FW.
  - UHAL_BASE  
      This is the address to be used for this AXI endpoint in the uHAL address table.   This can be "auto" if you want the uio_daemon to find a place for it when the blade comes up. (recommended)
  - HDL  
      If this node is specified, then a VHDL decoder is generated to bridge the axi bus to a set of packages needed.  If there is `map_template` child, this will override the vhdl template used for building the decoder.  If the `out_name` child is specified, then its value will be used when naming the entity and the output file so that it is easier to re-use code between flavors of FW. (ex. CM_FW_INFO used for F1_FW_INFO and F2_FW_INFO so that the code can reference the same CM_FW_INFO name, but the address table will differentiate the two)
      

Ex 1:  
```
AXI_SLAVES:
  LHC_CLK:
    TCL_CALL:
      command: ADD_PL_CLK
      name: LHC
      freq: 320
      global_signal: true
      add_rst_n: true
    SUB_SLAVES:
      AXI_RESET:
        TCL_CALL: make_bd_pins_external -name SYS_RESET_aux_rstn [get_bd_pins SYS_RESET/aux_reset_in]
      USER_AXI_RESET:
        TCL_CALL: make_bd_pins_external -name SYS_RESET_user_rst [get_bd_pins SYS_RESET/mb_debug_sys_rst]
      LHC_SYS_RESET:
        TCL_CALL:
          command: IP_SYS_RESET
          external_reset_n: ${::LHC_RSTN}
          aux_reset_n: ${::AXI_MASTER_RSTN}
          slowest_clk: ${::LHC_CLK}
        SUB_SLAVES:
          LHC_INTERCONNECT:
            TCL_CALL:
              command: BUILD_CHILD_AXI_INTERCONNECT
              axi_clk: ${::LHC_CLK}
              axi_rstn: "/LHC_SYS_RESET/interconnect_aresetn"
              parent:  ${::CM_INTERCONNECT}
              master_clk: ${::AXI_MASTER_CLK}
              master_rstn:  "/SYS_RESET/interconnect_aresetn"
            SUB_SLAVES:
```
Ex 2:
```
         .
         .
         .
         SERV:
            TCL_CALL:
              command: "AXI_PL_DEV_CONNECT"
              axi_control: "${::AXI_LOCAL_CTRL}"
            XML: "address_table/modules/SERV_rev2a.xml"
            UHAL_BASE: 0x04000000
            HDL:
              map_template: "axi_generic/template_map.vhd"
          SLAVE_I2C:
            TCL_CALL:
              command: "AXI_PL_DEV_CONNECT"
              axi_control: "${::AXI_LOCAL_CTRL}"
              addr:
                offset: -1
                range: "8K"
            XML: "address_table/modules/SLAVE_I2C.xml"
            UHAL_BASE: 0x05000000
         .
         .
         .
```

#### CORES  

