# Programming SM Clock Synthesiser

SM has a clock synthesiser used for the TCDS2/LHC-clocks that are passes to the CM. We have to manually program this chip after each cold (from IPMC) reset until this functionality is added in the bootup sequence.

Use the following [utility](https://cernbox.cern.ch/files/link/public/doSg8FL26cSIz2z?tiles-size=1&items-per-page=100&view-mode=resource-table) to program the SM synth. These commands can be executed from anywhere on the board.

```
wget https://cernbox.cern.ch/remote.php/dav/public-files/doSg8FL26cSIz2z/apollo_sm_clocks.zip
unzip apollo_sm_clocks.zip
cd clocks/
./clock_sync_320M_LHC CONFIGS/CONFIG.toml
```
