# Cheat-sheets, Tips and Tricks

## Updating the SM
  - [SM Firmware updating](../SM_Firmware/06-Updating_Firmware.md)
  - [SM filesystem updating](../SM_Filesystem/02-build.md)

## Bringing up the board
After the new FS and FW was installed on a board, the following steps are required to configure it:

  - Program MCU and set up the corresponding address tables using these [instructions](../CM_FW/CM_MCU/Installation.md)
  -  `reboot` the board 
  - Program the FPGAs on Command Module with [uio-daemon](https://gitlab.com/apollo-lhc/soc-tools/bootup-daemon#uio-client-operations). This tool allows to upload a fw tarball, power Up and program FPGA. Additionally, it manages address tables and uio-devices for SM and CM.

When developing and testing directly on a board, there are two registers address fields:
 - Using [BUTool](03-BUTool_Tutorial.md), which is coming pre-installed with the FS, and is using address tables defined in the `/opt/address_table/connections.xml` connection file
 - Using `emp-butler` from [emp-toolbox](https://gitlab.cern.ch/p2-xware/software/emp-toolbox/-/tree/master?ref_type=heads#build-instructions) with the `/opt/address_table/emp_connections.xml` connection file. This SW package does not come pre-installed and has to be build and installed in `/opt/`. Be aware that `python3.8` has to be used when building since our boards currently are using Alma8. When developing [apollo-herd](../SM_Software/Apollo_Herd/Introduction.md), installation of `emp-toolbox` is not needed. It comes pre-installed in the docker image.    

## Programming the CM
  - [One-shot](../Command-Module/04-FW_Loading_internals.md/#loading-cm-firmware-in-one-go)

## Slot Numerology


| Physical Slot | Logical Slot | IPMB-L Address |
| ------------- | ------------ | -------------- |
| 1  | 13 | 0x9A |
| 2  | 11 | 0x96 |
| 3  | 9  | 0x92 |
| 4  | 7  | 0x8E |
| 5  | 5  | 0x8A |
| 6  | 3  | 0x86 |
| 7  | 1  | 0x82 |
| 8  | 2  | 0x84 |
| 9  | 4  | 0x88 |
| 10 | 6  | 0x8C |
| 11 | 8  | 0x90 |
| 12 | 10 | 0x94 |
| 13 | 12 | 0x98 |
| 14 | 14 | 0x9C |


## Hardware inventory at BU

Click [**here**](http://ohm.bu.edu:8000/hardware)

## MPI CM specific info

### Access the MCU console

From ``BUTool``, enter ``uart_term CM_1``
    
### Programming the Kintex
   
* Warning!  When reprogramming an FPGA on a running CM, first
  ``cmpwrdown`` and then ``cmpwrup`` in BUTool

* After powering up the CM, enable C2C communications	
```
cd /home/atlas/mdttp-cm-demo-mcu-main/Software/HwTest/pyMcu
./script/setup_sm-cm.sh
```

* Program the Kintex from ``BUTool`` (example svf file shown here)

```
svfplayer /fw/CM/MPI_rev1_p1_KU15p-2-SM_7s/bit/top_MPI_rev1_p1_KU15p-2-SM_7s.svf
unblockAXI
```


