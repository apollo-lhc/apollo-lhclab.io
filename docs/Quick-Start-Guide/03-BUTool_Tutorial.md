# BUTool Tutorial

##BUTool: Communicating with Apollo Boards

Alp Akpinar

June 27, 2022

## Outline

* [BUTool](https://github.com/BU-Tools/BUTool/tree/develop) is an open\-source tool for interacting with hardware via device specific plugins
* Device plugin code can be broken into two parts:
    * ```c++``` class with basic and high\-level functionality \(to be used by SC/DAQ software\)
    * ```c++``` device class that wraps that functionality\, so that it is used in BUTool
* An Apollo specific [plugin](https://github.com/apollo-lhc/ApolloSM_plugin/tree/develop) is used to interact with Apollo SM and CM hardware 
    * Uses [UIOuHAL](https://github.com/BU-Tools/UIOuHAL/tree/develop) plugin for _µHAL_ hardware access
* Following functionalities are supported:
    * Read / write to registers
    * Display status tables for the Apollo hardware
    * Program FPGAs on the board using svf files
    * Others
* These slides: Quick overview of the usage & functionalities of BUTool on Apollo hardware

## Connecting to Boards at BU

* Multiple available Apollo boards to use at BU
    * Full list available at the next slide
* To connect
    * First ```ssh``` to the ```tesla``` machine at BU
        * You can ask Dan Gastler for an account on this device
    * From the ```tesla``` machine: ```ssh``` to the ```cms``` or ```atlas``` account on the board you want
* For this tutorial, we’ll use the ```SM02``` board, here are the steps to connect

![](img/jb_tut0.png)

![](img/jb_tut1.png)

* Before using one of the boards, make sure to check apollo\_access Slack channel
* If no one else is using it, please write there before taking the board

![](img/jb_tut2.png)

## Configuration: the Address Table

* These are XML files that store information about each register:
    * Each node represents an address to read/write
    * Read/write permissions
    * Additional parameters to be used by BUTool
* Stored under the path ```/opt/address_table``` on ```SM02``` blade
* More details about individual nodes in backup

![](img/jb_tut3.png)

![](img/jb_tut4.png)

## Running BUTool

* BUTool is started using the ```BUTool.exe``` command
    * The ```-a``` argument will load in an Apollo device with the given address table file

![](img/jb_tut5.png)

* Alternatively, start ```BUTool.exe``` without any command line arguments and add an APOLLOSM device
* Add the device manually using ```add_device`` command
    * ```add_device <device_type> <address_table.xml>```

![](img/jb_tut6.png)

Type ```help``` to get a list of available commands

![](img/jb_tut7.png)

## Running BUTool: Board Status

* The  ```status```  command is very useful to read registers that have information on board status, and display them in nice table formats
    * Firefly and FPGA temperatures
    * Information about firmware
    * Information about command modules, etc.
* The tables are defined in the address table
    * Nodes will have a ```"Table=<tableName>"``` parameter
* The  ```status```  command can be more verbose when given a different status level:
    * 1 = least verbosity, 9 = most verbosity
    * You can execute  ```status N``` where 1≤_N_≤9
    * _N_=1 is the default

You can also just read a single table: ```status <statusLevel> <tableName>```

![](img/jb_tut8.png)

## Running BUTool: Reading Registers

* BUTool also has the functionality to directly read registers by their names
    * The ```read``` command will return the raw 32-bit values in hex

![](img/jb_tut9.png)

## Running BUTool: Writing To Registers

* Like ```read```, you can write to the registers using the ```write``` command
    * Let’s get some dummy registers to try this:

![](img/jb_tut10.png)

## Running BUTool: Programming FPGAs

* ```SM02``` has two FPGAs, the Kintex and Virtex
* Now, le's program the Kintex!
    * First, we need to power up the command module

![](img/jb_tut11.png)

* With the FPGAs powered up, we can program the Kintex using an svf file
* The command is ```svfplayer```

![](img/jb_tut12.png)

* After the previous steps, try to read the ```CM_K_INFO``` registers
* And we have access (no ```BUS_ERROR```)!

![](img/jb_tut13.png)

* Now that we're done, we can power down the CM and quit the program
* The CM should be powered down before new firmware is loaded
    * Done to minimize current changes in the FPGA transceivers on rev1 CMs

![](img/jb_tut14.png)

## Running BUTool: Generic IPBus Devices

* BUTool source code is device-agnostic
    * Via device-specific plugins, BUTool can be used to interact with other IPBus devices as well
* A plugin is developed for interacting with Generic IPBus devices ([source](https://github.com/BU-Tools/GenericIPBus_plugin/tree/releases/v1.0))

![](img/jb_tut15.png)

## Summary

* A quick overview of key BUTool functionalities was presented
    * ```SM02``` board at BU is used for this tutorial
* Key information related to registers is stored in the address table
* After connecting to the board, using BUTool, we can
    * Read and write to registers
    * Check overall status of the board
    * Power up the command module and program the FPGAs
* BUTool source code is device-agnostic:
    * Using GenericIPBus device plugin, BUTool can be used to interact with other IPBus HW

## Bonus slides

### Configuration: The Address Table

* An example from ```PL_MEM.xml``` file
* Each register is represented as a node:
    * ```mask```: The bits to read for this register
    * ```permission```: Read \+ write permissions for this address
    * ```parameters```: Other parameters used by BUTool software (mostly Status Display tables, we’ll come to that)
    * Also, an optional description field

![](img/jb_tut16.png)

