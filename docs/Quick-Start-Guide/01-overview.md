# Overview

Resources to help newcomers get up to speed or for developers to quickly navigate to the needed chapters of this documentation. 
## This intro page has to be updated and extended, therefore considered as WIP. Feel free to submit an issue if you think of a relevant section here.

## F.A.Q

<details>
  <summary>How to power up/down the CM?</summary>
  <p>
    Most of the CM components can be powered up/down with this command:
  </p>
  <pre><code class="language-bash">
  BUTool.exe -a
  >cmpwrup 1
  >cmpwrdown 1
  </code></pre>
  <p>
    This will power up the FPGAs, FireFlies, clock chips (synthesizers), etc. Only MCU power is managed separately.
  </p>
  <p>
    Refer to this manual for more practical tips on 
    <a href="https://apollo-lhc.gitlab.io/Quick-Start-Guide/03-BUTool_Tutorial/#running-butool-reading-registers">BUTool</a>, or to 
    <a href="https://apollo-lhc.gitlab.io/SM_Software/bu-tool/">this one</a> for more.
  </p>
</details>

<details>
  <summary>How to powercycle the CM MCU?</summary>
  <p>
    Restart the MCU from the Zynq as follows in BUTool:
  </p>
  <pre><code class="language-bash">
  write CM.CM_1.CTRL.ENABLE_UC 0
  write CM.CM_1.CTRL.ENABLE_UC 1
  </code></pre>
</details>

<details>
  <summary>The board has crashed and is not replying, how do I powercycle it?</summary>
  <p>
    Powercycle from IPMC. The manual is available 
    <a href="https://apollo-lhc.gitlab.io/IPMC/01-ipmc-terminal/">here</a>.
  </p>
</details>

<details>
  <summary>How to use HERD sw on the board?</summary>
  <p>
    We have apollo-herd documentation available 
    <a href="https://apollo-lhc.gitlab.io/SM_Software/ApolloHerd/SetUp/">here</a>, as well as the README of the plugin 
    <a href="https://gitlab.cern.ch/cms-tracker-phase2-onlinesw/apollo-herd">repo</a>.
  </p>
  <p>
    Since this project is under active development, some instructions might be outdated and can be clarified with developers.
  </p>
</details>

<details>
  <summary>How to use EMP fw/sw on the board?</summary>
  <p>
    EMP-toolbox might be useful to debug some features with CLI. Follow 
    <a href="https://apollo-lhc.gitlab.io/SM_Software/emp-toolbox/">this guideline</a> to install it on the board.
  </p>
</details>

## Board components


# Apollo board
Apollo board has two main components: Service Module and Command Module

![](../images/apollo_map_detailed.jpeg)


## Command Module

### FPGA
CM hosts two main FPGA and optical modules for the upstream/downstream communication. 
To program FPGAs, login onto the board and follow these [instructions](https://apollo-lhc.gitlab.io/Command-Module/04-FW_Loading_internals/).

### LpGBT and copper links
Optical/copper links description/refs
* Per FPGA 52 optical FireFly ch (3x12 channel, 4x4 channel) 
* 52 copper inter-pga links
* FPGA1 <—> TCDS2


### Clock synthesisers and distribution
FPGA1 receives external clocks/signals (TCDS2) from the SM and relays it to FPGA2
External clocks infrastructure and distribution is described [here](https://apollo-lhc.gitlab.io/Command-Module/05-Clocks/).

### MCU
Microntroller unit (MCU) controls the CM components. Refer to the [CM manual](https://apollo-lhc.gitlab.io/cornell-cm/Manual/Microcontroller%20Information/01-NewFirmware/) for instructions on CLI tools and info. Additionally, [this page](https://apollo-lhc.gitlab.io/CM_FW/CM_MCU/Installation/) describes the process of FW update and address tables installation.
SM can read these registers and access MCU through the CLI, but no direct access from the SM FW yet. Development of this feature is being considered.



## Service Module

### SoC
SM has Zynq System-on-Chip: Enclustra ME-XU8-7EV-21-D12E-R2-1 
Custom-made Linux filesystem(FS) is mounted on μSD or EMMC. SSD support is currently under testing and will be released in the next FW tag.

### SoC FW
Zynq FW and kernel is in this [repo](https://gitlab.com/apollo-lhc/FW/SM_ZYNQ_FW), which also has a CI configured for builds. More info on the structure, builds and deployment is on this [page](https://apollo-lhc.gitlab.io/SM_Firmware/01-location/).

### SoC OS
OS(FS) that runs on Zynq is build from this [repo](https://gitlab.com/apollo-lhc/soc-os). This [page](https://apollo-lhc.gitlab.io/SM_Filesystem/01-location/) has the OS tags published with instructions on deployment. Similarly to the FW updating, one can use [apollo updater](https://gitlab.com/apollo-lhc/soc-tools/apollo-updater) to deploy FS on already running board. 

### Expert UART interface for control and status??

#### Open IPMC module
SM hosts an IPMC module that interacts with the shelf and has the following functionality:
* Reading and reporting sensors from SM and CM
* Power up/down and rebooting ZYNQ

More details can be found [here](https://apollo-lhc.gitlab.io/IPMC/01-ipmc-terminal/).

#### High-speed connectors to CM
* AXI bus via Chip2Chip connection, 10G link
* LHC clocks
* TCDS 10.24G link

#### Low-speed connector to CM
* JTAG - for FPGA programming and debugging via XVC
* 2 Serial Ports for information exchange between ZYNQ and CM MCU
* I2C bus for sensor readout and control
* General Purpose IOs

#### Ethernet Switch
* Two gigabit connections via backplane
* Connections to ZYNQ and IPMC modules

#### ATCA Power entry module: 12V DC-DC converter



