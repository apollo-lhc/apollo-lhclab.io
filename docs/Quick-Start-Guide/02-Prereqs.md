# Prerequisites

## ATCA shelf
- Any ATCA shelf should work. 
- For Rev1 Apollo SMs, the RTM cover should be removed so there is space for the loop back ethernet cable. 
- An ATCA ethernet switch should be installed for remote access of the Zynq and IPMC
- If no ethernet switch is available, one can connect ethernet to the zynq in the RTM zone of the Apollo SM
- It is also helpful to have a remotely accessible computer nearby with a USB cable to connect to the front panel console if needed.
- Remote access via the internet is needed for support. 

## Computer accounts (BU)
- Ask Dan in slack for a tesla.bu.edu account to use to hop over to the blades. 
- The currently active Blades at BU can be found [here](https://ohm.bu.edu/~apollo/shelf_status.html)

h