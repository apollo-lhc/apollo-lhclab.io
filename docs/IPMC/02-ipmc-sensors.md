# IPMC Sensors

The IPMC (Open or otherwise) is responsible for the readout and monitoring of
several sensors on both the command and service modules.

IPMC monitoring is done through two I2C buses:

## 1. Management I2C Bus

| Device                                 | Address      | Notes                                       |
|----------------------------------------|--------------|---------------------------------------------|
| PIM400KZ[^pim400]                      | 0b0101\_111X | DC/DC Converter Temperature/Voltage/Current |
| M24512-DFDW6TP[^eeprom] Memory         | 0b1010\_000X | EEPROM Memory Array                         |
| M24512-DFDW6TP[^eeprom] Identification | 0b1011\_000X | EEPROM Identification Page                  |

### PIM400

The PIM400KZ (datasheet at [^pim400]) is responsible for input power conversion/isolation.

It provides I2C accessible sensors for Temperature, Voltage, and Current.

### EEPROM

The EEPROM is used to store non-volatile board-specific information.

It currently holds a data structure consisting of:

``` c
uint8_t version;
uint8_t serial_number;
uint8_t revision_number;
uint8_t mac_eth0[6];
uint8_t mac_eth1[6];
uint8_t boot_mode;
```

Please see open_ipmc `user_eeprom.c` for the implementation. The IPMC is
responsible for reading and writing to the EEPROM. These parameters can be set
in the OpenIPMC through the Telnet interface. Upon startup, they are read from
the EEPROM and stored in the OpenIPMC memory.

Some of the values are also copied to the Zynq (written into its I2C target).

Please see the Zynq I2C Interface section for further details.

## 2. Sense I2C Bus

The sense I2C bus is multiplexed through a TCA9546A[^tca] I2C multiplexer, which
allows selection of any of four output channels:

1. Local I2C
2. CM1 I2C
3. CM2 I2C
4. Zynq I2C

The TCA9546 is accessible at address `0b1110_000X`, and individual channels can
be selected by writing to a 4-bit bitmask.

### Local I2C

| Device           | Address      | Notes                                             |
|------------------|--------------|---------------------------------------------------|
| TCN75A[^tcn] Top | 0b1001\_000X | Temperature measurement for Service Module Top    |
| TCN75A[^tcn] Mid | 0b1001\_001X | Temperature measurement for Service Module Middle |
| TCN75A[^tcn] Bot | 0b1001\_010X | Temperature measurement for Service Module Bottom |

### CM I2C

Note that for historical reasons, there are two I2C buses connected to the same
Command Module. This is because in earlier visions of the Apollo platform, the
two FPGAs of the Command Module were foreseen to be on independent boards, and
hence have separate LV/I2C/Clocking/AXI connections. In reality only
"double-wide" command modules were built, so all of the sensors can be read from
the single CM1 I2C bus.

The CM I2C sensors are well documented in Peter's wiki:

- <https://github.com/apollo-lhc/cm_mcu/wiki/MCU-I2C-target-documentation>

The I2C target is the CM microcontroller, which reads/monitors all sensors
on the CM. It reports back to the IPMC only a subset of these sensors,
corresponding to:

1. FPGA 1 temperature
2. FPGA 2 temperature
3. Hottest firefly temperature
4. Hottest regulator temperature
5. MCU temperature

### Zynq I2C

One of the IPMC I2C masters connects to an I2C target on the Zynq.

The Zynq exposes multiple I2C targets at different addresses on the same pins.

Their address tables are as follows:

#### S1: Address = 0x60

| Reg Addr [7:0] | Mask | Name                           | Notes                                                                          |
|----------------|------|--------------------------------|--------------------------------------------------------------------------------|
| 0x00           | 0x01 | S1.SM.STATUS.DONE              | 1 indicates that Zynq Linux has finished booting                               |
| 0x00           | 0x02 | S1.SM.STATUS.ERROR             | 1 indicates a Zynq Linux error during boot                                     |
| 0x00           | 0x04 | S1.SM.STATUS.ZYNQ\_DONE        | Unused                                                                         |
| 0x00           | 0x10 | S1.SM.STATUS.SHUTDOWN\_REQUEST | Set to 1 for Linux to shut down (set by IPMC)                                  |
| 0x00           | 0x20 | S1.SM.STATUS.SHUTDOWN          | 1 indicates Linux heartbeat has timed out and Linux can be considered shutdown |
| 0x04           | 0xFF | S1.SM.INFO.SN                  | SM blade serial number (set by IPMC)                                           |
| 0x05           | 0xFF | S1.SM.INFO.SLOT                | Slot number for blade (set by IPMC) (the useful number, not the divide by 2!)  |
| 0x07           | 0xFF | S1.SM.INFO.RN                  | Hardware revision for SM blade                                                 |
| 0x08           | 0xFF | S1.SM.INFO.SHELF               | Shelf ID LSB                                                                   |
| 0x09           | 0xFF | -                              | Shelf ID MSB                                                                   |
| 0x14           | 0xFF | S1.SM.ETH0\_MAC.BYTE\_0        | Ethernet 0 MAC Addr Byte0 (set by IPMC) |
| 0x15           | 0xFF | S1.SM.ETH0\_MAC.BYTE\_1        | Ethernet 0 MAC Addr Byte1 (set by IPMC) |
| 0x16           | 0xFF | S1.SM.ETH0\_MAC.BYTE\_2        | Ethernet 0 MAC Addr Byte2 (set by IPMC) |
| 0x17           | 0xFF | S1.SM.ETH0\_MAC.BYTE\_3        | Ethernet 0 MAC Addr Byte3 (set by IPMC) |
| 0x18           | 0xFF | S1.SM.ETH0\_MAC.BYTE\_4        | Ethernet 0 MAC Addr Byte4 (set by IPMC) |
| 0x19           | 0xFF | S1.SM.ETH0\_MAC.BYTE\_5        | Ethernet 0 MAC Addr Byte5 (set by IPMC) |
| 0x1C           | 0xFF | S1.SM.ETH1\_MAC.BYTE\_0        | Ethernet 1 MAC Addr Byte0 (set by IPMC) |
| 0x1D           | 0xFF | S1.SM.ETH1\_MAC.BYTE\_1        | Ethernet 1 MAC Addr Byte1 (set by IPMC) |
| 0x1E           | 0xFF | S1.SM.ETH1\_MAC.BYTE\_2        | Ethernet 1 MAC Addr Byte2 (set by IPMC) |
| 0x1F           | 0xFF | S1.SM.ETH1\_MAC.BYTE\_3        | Ethernet 1 MAC Addr Byte3 (set by IPMC) |
| 0x20           | 0xFF | S1.SM.ETH1\_MAC.BYTE\_4        | Ethernet 1 MAC Addr Byte4 (set by IPMC) |
| 0x21           | 0xFF | S1.SM.ETH1\_MAC.BYTE\_5        | Ethernet 1 MAC Addr Byte5 (set by IPMC) |
| 0x24           | 0xFF | S1.SM.TEMP.TEMP                | Current Zynq temp C |
| 0x28           | 0x01 | S1.SM.CM.KINTEX\_PGRM\_DONE    | (not used)                                                                     |
| 0x28           | 0x10 | S1.SM.CM.VIRTEX\_PGRM\_DONE    | (not used)                                                                     |

#### S7: Address = 0x66

| Reg Addr [7:0] | Mask | Name                    | Notes                                 |
|----------------|------|-------------------------|---------------------------------------|
| 0x00           | 0xFF | S8.PIM.STATUS           | Status bits (broken out in xml)       |
| 0x04           | 0xFF | S8.PIM.V\_HOLDUP        | Holdup Voltage                        |
| 0x05           | 0xFF | S8.PIM.I\_OUT           | Current out                           |
| 0x06           | 0xFF | S8.PIM.V\_IN\_A         | Vin A                                 |
| 0x07           | 0xFF | S8.PIM.V\_IN\_B         | Vin B                                 |
| 0x08           | 0xFF | S8.PIM.TEMP             | Module Temperature                    |
| 0x14           | 0xFF | S8.PIM.SM_TEMP.TOP_0    | Top temp LSB                          |
| 0x15           | 0xFF | S8.PIM.SM_TEMP.TOP_1    | Top temp MSB                          |
| 0x16           | 0xFF | S8.PIM.SM_TEMP.MID_0    | Mid temp LSB                          |
| 0x17           | 0xFF | S8.PIM.SM_TEMP.MID_1    | Mid temp MSB                          |
| 0x18           | 0xFF | S8.PIM.SM_TEMP.BOTTOM_0 | Bottom temp LSB                       |
| 0x19           | 0xFF | S8.PIM.SM_TEMP.BOTTOM_1 | Bottom temp MSB                       |

#### S8: Address = 0x67

| Reg Addr [7:0] | Mask | Name                    | Notes                                 |
|----------------|------|-------------------------|---------------------------------------|
| 0x00           | 0xFF | S8.IPMC\_MAC.BYTE\_0    | IPMC MAC Address Byte 0 (set by IPMC) |
| 0x01           | 0xFF | S8.IPMC\_MAC.BYTE\_1    | IPMC MAC Address Byte 1 (set by IPMC) |
| 0x02           | 0xFF | S8.IPMC\_MAC.BYTE\_2    | IPMC MAC Address Byte 2 (set by IPMC) |
| 0x03           | 0xFF | S8.IPMC\_MAC.BYTE\_3    | IPMC MAC Address Byte 3 (set by IPMC) |
| 0x04           | 0xFF | S8.IPMC\_MAC.BYTE\_4    | IPMC MAC Address Byte 4 (set by IPMC) |
| 0x05           | 0xFF | S8.IPMC\_MAC.BYTE\_5    | IPMC MAC Address Byte 5 (set by IPMC) |
| 0x08           | 0xFF | S8.IPMC\_IP.BYTE\_0     | IPMC IP Address Byte 0 (set by IPMC)  |
| 0x09           | 0xFF | S8.IPMC\_IP.BYTE\_1     | IPMC IP Address Byte 1 (set by IPMC)  |
| 0x0A           | 0xFF | S8.IPMC\_IP.BYTE\_2     | IPMC IP Address Byte 2 (set by IPMC)  |
| 0x0B           | 0xFF | S8.IPMC\_IP.BYTE\_3     | IPMC IP Address Byte 3 (set by IPMC)  |

#### Zynq I2C Notes

Communication with the Zynq I2C slave must not have a stop/start between bytes.

An example waveform of successful communication is:

#### Write
![Zynq Write Transaction](i2c-write.png)

#### Read
![Zynq Read Transaction](i2c-read.png)

[^pim400]: PIM400 Datasheet: <https://www.mouser.com/datasheet/2/167/PIM400_DS-1920379.pdf>
[^eeprom]: M24512-DFDW6TP Datasheet: <https://www.st.com/content/ccc/resource/technical/document/datasheet/e0/4c/87/45/e8/bd/4f/11/CD00251873.pdf/files/CD00251873.pdf/jcr:content/translations/en.CD00251873.pdf>
[^tca]: TCA9546A Datasheet: <https://www.ti.com/lit/ds/symlink/tca9546a.pdf>
[^tcn]: TCN75A Datasheet: <https://ww1.microchip.com/downloads/en/DeviceDoc/21935D.pdf>
