# Using Saleae Logic Analyzer

To take a detailed look at the I2C transactions between the OpenIPMC and several components of Apollo boards,
a logic analyzer can be hooked to the board. This section explains:

* How to plug the logic analyzer (Saleae) device in
* How to install the logic analyzer software on the host machine
* How to take I2C transaction data and configure the logic analyzer

## Plugging the Logic Analyzer Device in

You can follow these steps to plug the logic analyzer device in:

* Plug the USB end of the logic analyzer device to a host computer, where the Saleae logic analyzer software is installed / will be installed.
* Plug in a jumper to connector *J14* on the Apollo Service Module, as shown below. The location of the jumper is highlighted in <span style="color:red">red</span>.

<div style="text-align: center;">
    <img src="../../images/saleae_logic_analyzer/sm_with_jumper.png" width=750 />
</div>

* With the jumper in place, plug in the other end of the logic analyzer device to the jumper on *J14* on the Apollo Service Module, as shown below.
You will need three wires in the following locations:

    - The left-most connector is connected to the **SDA line**. Here, use one of the colored wires and take note of the corresponding channel number (labeled in the back of the Saleae device).
    - The middle connector is connected to the **SCL line**. Here, also use one of the colored wires and take note of the corresponding channel number.
    - The right-most connector is connected to the **ground**. For this one, use one of the black ground wires, connected to the second row of Saleae device, and is labeled as *G*.

<div style="text-align: center;">
    <img src="../../images/saleae_logic_analyzer/sm_with_saleae_plugged.png" width=750 />
</div>

**Note:** Please note that the *J14* is connected to the **sensor bus** (or also called **sense I2C bus**) on the Apollo Service Module. Hence, a logic analyzer connected
to here will allow to monitor the usage of this bus. It will **not** allow to monitor the usage on the other main bus, which is the management I2C bus.

Please make sure that the jumper and the wires are hooked tightly to *J14*. 

## Installing the Saleae Software

Once the logic analyzer is plugged in on both ends, the next step is to install the Saleae logic analyzer software which will sample data. This should be installed on the machine where
the logic analyzer is connected via USB.

To install the logic analyzer software, you can follow these steps:

* Go to Saleae's website [here](https://www.saleae.com/downloads/) for downloads, at the bottom menu, select *Download for Linux*.
* Click on the *Download for Linux* button that appears on the webpage. This should install a file called `Logic-2.x.xx-master.AppImage` 
to your host machine.
* Once the file is downloaded, make it executable with `chmod +x Logic-2.x.xx-master.AppImage`.
* Launch the logic analyzer by running the executable `./Logic-2.x.xx-master.AppImage`. You should see a start screen that looks like the one shown below.

<div style="text-align: center;">
    <img src="../../images/saleae_logic_analyzer/saleae_start_screen.png" width=1000 />
</div>

* If you have problems running the software, please consult the install instructions from Saleae [here](https://support.saleae.com/logic-software/sw-installation#ubuntu-instructions).

## Running the Logic Analyzer

Once the logic analyzer device is plugged in and Saleae software is installed on the connecting host machine, you are ready to sample I2C
transactions on Apollo!

### Add Channels

You need to select which channels of the logic analzyer device are plugged in add them to the panel. To do this, you can:

* Go to *Device Settings* on the right-hand side menu.
* Under *Digital*, click on the two channels (one for SDA, one for SCL) you plugged in to Apollo. Please note that if the channel numbers do not match with
what you plugged in, you will not be able to take data.
* Under *Digital*, make sure that the voltage level is set to *3.3+ Volts*.
* If you want to see analog signals as well, you can pick the same channels on *Analog* menu under.

<div style="text-align: center;">
    <img src="../../images/saleae_logic_analyzer/saleae_device_settings.png" width=1000 />
</div>

### Configure Trigger

On the same menu, you need to configure the trigger, which will tell Saleae on what type of transaction to be triggered, and eventually stop sampling.

* Still on *Device Settings* menu, on the bottom half, select *Trigger*.
* Under *Pattern and Channel*, pick the channel for the trigger, and pick the event as *Falling Edge*. This way, we trigger on an I2C start condition.
* Under *Capture duration after trigger*, you can specify for how long Saleae should keep taking data after the trigger condition. 

<div style="text-align: center;">
    <img src="../../images/saleae_logic_analyzer/saleae_trigger_config.png" width=250 />
</div>


### Configure I2C Analyzer

Next step is to configure the I2C analyzer, so that the Saleae software can interpret the bits as I2C transactions, and will add (more) human-readable labels per transaction.
To do this, you can:

* Go to *Analyzers* section on the right-hand side menu.
* Click on the &plus; button at the top right to add a new I2C analyzer. 
* From the drop-down menu choose *I2C*.
* In the pop-up window, pick the channels for the SDA and SCL.
* Click *Save*.

<div style="text-align: center;">
    <img src="../../images/saleae_logic_analyzer/saleae_i2c_analyzer.png" width=500 />
</div>

### Sample I2C Transactions

With all the above steps done, you should be good to go to sample I2C transactions on Apollo using Saleae logic analyzer! To start taking data, click on *Start* button
<img src="../../images/saleae_logic_analyzer/start_button.png" width=40 /> on the top-right.

An example I2C transaction is shown below.

<div style="text-align: center;">
    <img src="../../images/saleae_logic_analyzer/example_i2c_transaction.png" width=1000 />
</div>

In summary, this transaction shows where the OpenIPMC is reading data from two of the TCN temperature sensors located on the Apollo Service Module. Here is a more detailed,
step-by-step description of what is happenning:

* OpenIPMC launches the transaction (as master device) by pulling the SDA line low, before SCL line goes low. This is known as an I2C *start condition* (Saleae highlights this with a 
<span style="color:green">green</span> dot).
* In the first transaction, OpenIPMC writes a value of *0x01* to the TCA9546A multiplexer device (mux), located at *0x70* on the bus, to select the bus that is connected to TCN sensors.
The *ACK* bits (SDA pulled down for 1 clock-tick) at the end of each 8-bits on SDA line confirms that the mux has received the message.
* The <span style="color:orange">orange</span> square shows the *stop condition*, where the SCL line goes up, followed by SDA line going up. This ends the first transaction between the
OpenIPMC and the mux.
* In the second transaction, the OpenIPMC reads 2-bytes of data from the TCN sensor located at *0x48* on the bus. It gets 2 bytes of data back, *0x1D* and *0x00*. 
* Notice that after sending 2 bytes of data to the OpenIPMC, the TCN sensor
device does not give an *ACK* bit, signaling that it has finished sending data over. This is shown here (and commonly referred to) as the *NAK* bit.
* In the third and final transaction, the OpenIPMC reads 2-bytes of data from another TCN sensor, located at *0x4A* on the bus. It gets 2 bytes of data back, *0x1F* and *0x00*.
