# IPMC LED Patterns

## ESM Power Bad

All LEDs on/off at 200 ms interval

<img width=50 src="../esm-power-bad.svg">

## Handle Open

<img width=50 src="../handle-open.svg">

## Board Shut down abormally

<img width=50 src="../abnormal-shutdown.svg">

## Waiting for Zynq FPGA

<img width=50 src="../waiting-for-fpga.svg">

## Waiting for Zynq CPU

<img width=50 src="../waiting-for-cpu.svg">

## Ready

<img width=50 src="../ready.svg">

## Waiting to startup

<img width=50 src="../not-started.svg">
