# IPMC Terminal Commands

This section contains information about OpenIPMC terminal commands, which can be accessed from the `telnet` command line
interface of the OpenIPMC. To access these commands, you'll need the IP address of the OpenIPMC, a nice website to check
IP addresses of OpenIPMCs at BU can be found [here](https://ohm.bu.edu/~apollo/shelf_status.html). Once you have the IP
address, just launch a `telnet` session from a computer within the EDF internal network:

```
telnet <OpenIPMC IP address>
```

Hit `Enter` a couple of times for the command prompt `>>` to appear. You can execute the `help` command to see the full
list of available commands and their documentation.

## Status Commands

| Command      | Description                                                     |
|--------------|----------------------------------------------                   |
| `info`       | Read IPMC info (fw version, slot, etc)                          |
| `bootloader` | Get the status of the IPMC bootloader                           |
| `bootstatus` | Get the status of the boot sequence                             |
| `debug-ipmi` | Debug output from OpenIPMC. Hit `ESC` key to exit the console.  |
| `pimrd`      | Read Apollo PIM400                                              |
| `tcnrd`      | Read Apollo TCN Temperature Sensors                             |
| `powerup`    | Power up Apollo                                                 |
| `powerdown`  | Power down Apollo                                               |
| `restart`    | Restart Apollo (disconnects network in rev1)                    |
| `readio`     | Read IPMC status IOs                                            |

## EEPROM Setting

| Command    | Description                                      | Notes                                             |
|------------|--------------------------------------------------|-----------------------------------------          |
| `dis_shdn` | 1 to disable IPMC shutdown if Zynq is not booted |                                                   |
| `sdsel`    | Set the Apollo SD select pin                     | 1=Midboard, 0=Front Panel                         |
| `bootmode` | Set the Apollo boot mode pin and write to EEPROM | 3=SD, 2=QSPI, 1=NAND, 0=JTAG                      |
| `idwr`     | Write SM serial number                           |                                                   |
| `revwr`    | Write SM board revision                          |                                                   |
| `verwr`    | Write EEPROM dataformat revision                 | (should be 0)                                     |
| `ethmacwr` | Write MAC addresses for ETH0 and ETH1 ports      | Usage: `ethmacwr 0/1 <space seperated MAC addr>`  |
| `eepromrd` | Read Apollo EEPROM (all values)                  |                                                   |

## I2C Commands for Debugging

I2C r/w commands can be used as e.g. `zwr c ff`

| Command  | Description              |
|----------|--------------------------|
| `i2csel` | Configure Apollo I2C Mux |
| `lrd`    | Read Apollo Local I2C    |
| `lwr`    | Write Apollo Local I2C   |
| `zrd`    | Read Apollo Zynq I2C     |
| `zwr`    | Write Apollo Zynq I2C    |
| `c1rd`   | Read Apollo CM1 I2C      |
| `c1wr`   | Write Apollo CM1 I2C     |
| `c2rd`   | Read Apollo CM2 I2C      |
| `c2wr`   | Write Apollo CM2 I2C     |
