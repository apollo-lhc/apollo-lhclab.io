# Publications for Apollo Project

## Hardware Platform

- "The Apollo ATCA Platform", E. Hazen _et al._, PoS **370** 120 (2020) doi:10.22323/1.370.0120 (TWEPP 2019)
- "The Apollo ATCA Design for the CMS Track Finder and the Pixel Readout at the HL-LHC," A. Albert _et al._, JINST **17** C04033 (2022), doi:10.1088/1748-0221/17/04/C04033 (TWEPP 2021)
- "Design, Construction, and Testing of the APOLLO ATCA Blades for Use at the HL-LHC", A. Albert _et al._, submitted to _Proceedings for the Topical Workshop for Electronics for Particle Physics 2024_. (TWEPP2024) [JINST_005P_1124](https://jinst.sissa.it/jinst/author/docPage.jsp?docId=JINST_005P_1124&docPgType=work), [arXiv: 2501.03702](https://arxiv.org/abs/2501.03702)

## Projects that use Apollo

### Inner Tracker DTC

### Track Finder

- "FPGA-based tracking for the CMS Level-1 trigger using the tracklet algorithm," E. Bartz et al., JINST 15 P06024 (2022), doi:10.1088/1748-0221/15/06/P06024
