# Software infrastructure
When the [File System](https://gitlab.com/apollo-lhc/soc-os) (FS) for the SoC OS is built, most of the SW we use is installed using rpms specified in this [list](https://gitlab.com/apollo-lhc/soc-os/-/blob/master/alma8-rootfs/extra_rpms.txt). In particular, we will focus here on the libraries and daemons developed for Apollo, such as ApolloSM, BUTool, UIODaemon, etc.

## Development and tests
Any push of changes to a development branch triggers a GitLab CI job that builds, packages and in some cases deploys the SW. One can retrieve job logs and artifacts using CI interface for the given repo. We can use [UIODaemon CI setup](https://gitlab.com/apollo-lhc/soc-tools/bootup-daemon/-/blob/main/.gitlab-ci.yml?ref_type=heads) as an example:

```yaml
.common:
  before_script:
    - yum -y install ApolloSM protobuf-devel bash-completion
  script:
    - make -j4
    - cd rpm
    - make
  artifacts:
    paths:
      - rpm/rpm/*

stages:
  - build
  - deploy
```

After the build is finished, software is packaged in an rpm, and is available as an artifact. The next step is to install it on the board and test functionality. In case of the uio-daemon, these steps are:

```bash
# download rpm from the CI job artefacts
curl -O https://gitlab.com/apollo-lhc/soc-tools/bootup-daemon/...../UIODaemonPlugin-v1.3.0_3_g7317b73-1.aarch64.rpm
# stop the daemon service of the previous version
systemctl stop uio_daemon.service
dnf install UIODaemonPlugin-v1.3.0_3_g7317b73-1.aarch64.rpm
# restart the daemon
systemctl daemon-reload
systemctl enable uio_daemon.service
systemctl start uio_daemon.service
```

The exact deployment procedure will depend on the library under development. But a special care has to be taken when deploying SW that already runs on the board (see the [BUTool case](https://apollo-lhc.gitlab.io/SM_Software/apollo-tool/#debugging-development))

## Dev containers, CI and yum repos

The second part of [CI configuration](https://gitlab.com/apollo-lhc/soc-tools/bootup-daemon/-/blob/main/.gitlab-ci.yml?ref_type=heads) describes steps of multiple SW builds. Software is compiled and packaged in docker containers. Docker images are built for each `arch` and OS `variant` and are stored in our [dockerhub](https://hub.docker.com/u/edfapollo) repo. These images have some minimally required tools preinstalled, but any application specific pre-requisites are added at the CI stage. Additionally, [edf yum-repository](http://ohm.bu.edu/repo/) is configured in the image, which makes possible to install tools as `ApolloSM` with `yum / dnf`.

```yaml
#
# RPM build jobs for AlmaLinux8
#

job_alma8_arm64v8:
  stage: build
  image: edfapollo/multiarch-butool-almalinux8:1.7-arm64v8
  extends:
    - .common
  before_script:
    - rpm --import https://repo.almalinux.org/almalinux/RPM-GPG-KEY-AlmaLinux
    - yum -y install ApolloSM protobuf-devel bash-completion

```

These docker images are built and published with our [build-containers](https://gitlab.com/BU-EDF/BUTool/build-containers) repo.

## Deployment

Finally, the [last part](https://gitlab.com/apollo-lhc/soc-tools/bootup-daemon/-/blob/main/.gitlab-ci.yml?ref_type=heads) shows the deployment stage:

```yaml
deploy_rpms:
  stage: deploy
  rules:
    - if: $CI_COMMIT_TAG
      when: on_success
  trigger: BU-EDF/edf-projects/yum-repo-builder

```

When the master branch of corresponding library being tagged, the [yum-repo-builder](https://gitlab.com/BU-EDF/edf-projects/yum-repo-builder) publishes this version to the [edf yum-repository](http://ohm.bu.edu/repo/). To use rpms from this repo, one has to configure it in the system by creating a repo file `/etc/yum.repos.d/edf_tools.repo`:

```
echo "[edf_tools]" > /etc/yum.repos.d/edf_tools.repo
echo "name=EDF SW tools" >> /etc/yum.repos.d/edf_tools.repo
echo "baseurl=http://ohm.bu.edu/repo/AlmaLinux/8/\$basearch/" >> /etc/yum.repos.d/edf_tools.repo
echo "enabled=1" >> /etc/yum.repos.d/edf_tools.repo
echo "gpgcheck=0" >> /etc/yum.repos.d/edf_tools.repo
```

And run:

```bash
dnf clean all
dnf makecache

dnf install package-name
```

To make newly developed package installed in the [SoC-OS](https://gitlab.com/apollo-lhc/soc-os) by default, one has to add this to a list of RPMs (preferably), or include the build and installation instructions in the chain of Makefiles.

## devconatiner in VSCode

Sometimes the process of passing the SW builds through the CI job can be too tedious, especially when the changes are small or compilation time is minimal. In that case, the build can be done on users PC (or directly on the board, when need to test the developed feature on fly) in the same docker container that is specified in the CI config, or using VSCode UI that wraps around the docker engine. To set that up, `.devcontainer/devcontainer.json` config has to be added to the repo, which will be detected by VSCode.

```json
{
    "image": "edfapollo/multiarch-butool-almalinux8:1.7-arm64v8",
    // "onCreateCommand": "ci/install_dependencies.sh dev",
    "workspaceMount": "source=${localWorkspaceFolder},target=/workspace/____________________/BOOTUP-DAEMON,type=bind,consistency=cached",
    "workspaceFolder": "/workspace/____________________/BOOTUP-DAEMON"
  }
  
```
