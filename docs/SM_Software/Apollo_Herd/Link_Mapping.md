# Link Mapping

This is a brief tutorial on how links on how to derive the link mapping from information on the FPGAs contained in the schematics and EMP device VHDL files. An example will be referenced to make this tutorial easier to follow. The completed link mapping for each FPGA can be found here for [F1](https://gitlab.cern.ch/cms-tracker-phase2-onlinesw/apollo-herd/-/blob/master/etc/PinInfo_Rev2b_VU13p_p1.yaml?ref_type=heads) and [F2](https://gitlab.cern.ch/cms-tracker-phase2-onlinesw/apollo-herd/-/blob/master/etc/PinInfo_Rev2b_VU13p_p2.yaml?ref_type=heads).

1. Access the schematic corresponding to your board revision. Here is the schematic for [Rev2b](https://github.com/apollo-lhc/Cornell_CM_Rev2_HW/blob/master/PCB_Rev2B/Schematic_6089-119-revb_13Feb2023.pdf).

2. In this example, FPGA 1's quads are visualized on the LHS while FPGA 2's quads are on the RHS. Find your desired quad and the one it is linked to. For example, quad A on F1 is connected to quad F on F2.

3. Access the corresponding EMP device VHDL files found on the emp-fwk repo. Since we are working with Rev2b boards that have VU13p FPGAs, the files are found here for [F1](https://gitlab.cern.ch/p2-xware/firmware/emp-fwk/-/blob/master/boards/apollo/cm_v2/vu13p/firmware/hdl/emp_device_decl_cm_v2_p1.vhd) and [F2](https://gitlab.cern.ch/p2-xware/firmware/emp-fwk/-/blob/master/boards/apollo/cm_v2/vu13p/firmware/hdl/emp_device_decl_cm_v2_p2.vhd).

4. Determine the EMP number for the desired FPGA from the EMP device VHDL files. The EMP number is the number in blue at the start of each line. This can be matched to the FPGA's letter or quad number by seeing the number and letter combination after the -- in gray. For example, quad A/225 on F1 corresponds to EMP number 5 and quad F/230 on F2 corresponds to EMP number 10.

5. Each quad has four channels available for a link. The EMP channel numbers can be derived from the sets of equations below, with slight differences for F1 and F2. The reverse in the order comes from the fact that the FPGAs have the opposite physical orientation on the board. 'FF channel' below takes on a value in the range [0,3] for the four channels.

F1 - EMP Channel = (EMP Number)*4 + (FF Channel)

F2 - EMP Channel = (EMP Number)*4 + 3 - (FF Channel)

6. After calculating the EMP channel numbers for each FPGA, we can then match the links that will be connected by their corresponding FF channel. For example, A on F1 has its FF channel 2 mapping to 22, while F on F2 has its FF channel 2 mapping to 41. As a result, the EMP channels 22 and 41 are connected. Doing this for the rest of the links, we see that for the quads A on F1 and F on F2, the following EMP links exist 20<->43, 21<->42, 22<->41, 23<->40.

This page was last updated - 1/17/2025