# FW and Link Testing
These tests are designed to assess the performance of FW and link integrity through algorithm and link testing. This page provides some tutorials, example files, and definitions of configurable parameters in these tests.

This page was last updated - 1/17/2025

# Algorithm Testing
A brief tutorial for replicating the null Algorithm testing is given below.

1. Identify the machine where you will run the Algorithm test. Either Server-room at BU or Aethon at the TIF.

2. Create a directory called `AlgoTest` which contains:
    - Input dummy data (see example scripts below)
    - FW for both FPGAs (see Jonni for latest FW)
    - A YAML file which registers necessary configuration information (see example scripts below)

3. From here, we can launch the podman image that initializes the algorithm test using (use docker if at the TIF)
    - The current algo-test-runner-image at the time of writing is: gitlab-registry.cern.ch/cms-cactus/phase2/pyswatch/centos/algo-test-runner:algo-test-runner-renew-lease-5f4edcec
    - Ask Tom Williams for an updated version if needed

    ```
    podman run --entrypoint bash -it --privileged -v ~/AlgoTest:/AlgoTest algo-test-runner-image
    ```

4. Once inside the algo-test-runner-image we can start the algorithm test
     - The enabled quad numbers in the example command are specifically for the channels corresponding to FFs
     - FULLY_QUALIFIED_BOARD_NAME can be (for example) apollo212-1.edf.lo (BU) or cmsapollo214.cern.ch (TIF)
     - FPGA_ID can be F1 or F2

    ```
    bash entrypoint.sh FULLY_QUALIFIED_BOARD_NAME FPGA_ID AlgoTest/ALGOTEST.YML AlgoTest/FPGA_FW AlgoTest/INPUT_DATA.TXT algoTestOutput.txt --input-channels 68-79,84-123 --output-channels 68-79,84-123
    ```

5. After the test finishes, we can check the output in algoTestOutput.txt. Verify that input and output data match between corresponding channels.

# Link Testing
## Herd 

A brief tutorial for replicating the herd link tests is given below.

1. Identify the machine where you will run the Link test. Either Server-room at BU or Aethon at the TIF. 

2. Create a directory called `LinkTest` which contains:
    - Input dummy data (see example scripts below)
    - FW for both FPGAs (see Jonni for latest FW)
    - A YAML file which registers necessary configuration information (see example scripts below)

3. From here, we can launch the podman image that initializes the link test using (use docker if at the TIF)
    - The current link-test-runner-image at the time of writing is: gitlab-registry.cern.ch/cms-cactus/phase2/pyswatch/centos-linux-amd64/link-test-runner:cleanup-1180cb60
    - Ask Tom Williams for an updated version if needed

    ```
    podman run --entrypoint bash -it --privileged -v ~/LinkTest:/LinkTest link-test-runner-image
    ```

4. Once inside the link-test-runner-image we can start the link test
    - Check-io-data automatically compares the input and output data to make sure the links are declared properly in Herd

    ```
    bash entrypoint.sh LinkTest/LINKTEST.YML --capture-path LinkTest/OUTPUTPATH.txt --check-io-data
    ```

## EMP Butler
A brief tutorial for replicating the EMP Butler link tests is given below.

1. Build EMP-Toolbox on the board you are working on. Follow the [instructions here](https://apollo-lhc.gitlab.io/SM_Software/emp-toolbox/) but make sure to check-out the [branch here](https://gitlab.cern.ch/p2-xware/software/emp-toolbox/-/tree/tcds2_full_implementation?ref_type=heads) for compatibility with TCDS2.

2. Program both FPGAs by running the command below using your desired FW for each FPGA.

    ```
    uio_send.py program-fpga -f yourFW.tar.gz
    ```

3. Enable EMP-Toolbox by cd'ing to the base directory for it and running

    ```
    source scripts/env.sh
    ```

4. Use BUTool to reset the clocks

    ```
    BUTool.exe -a
    write SERV.CLOCKING.HQ_SEL 0
    write SERV.TCDS.TTC_SOURCE 0
    ```

5. Write and run a bash script containing the different commands to execute the link test in EMP butler (see examples below)

6. Ascertain whether the injected_data, rx_summary, and tx_summary text files outputted by the link tests are consistent with what we expect. 
    - Specifically in tests where we use --loopback=nearPMA, you can use this [script here](https://gitlab.cern.ch/cms-tracker-phase2-onlinesw/apollo-herd/-/blob/master/FW_Link_Tests/CompareInjectedCaptured.py?ref_type=heads) to do a quick comparison. 
    - Note that in all cases where one uses PRBS, no information is passed to the rx buffers, so manual comparison with the injected data will not make sense. 


# Link Test Variations
As specified in the [link test Google sheet](https://docs.google.com/spreadsheets/d/1cC8sIu3WxagOKCyWCFojkVc1GVEKO8q5Py-EkMlObwI/edit?gid=472488064#gid=472488064), there are many variations in the parameters/configuration of the link tests. This subsection defines what many of these are and their corresponding use cases. 

## Links
- **Copper**: 
    - These links are static and built-in to the board. They facilitate cross-FPGA communication with the starting and end-points both being on one of the two FPGAs. 
    - Please note that Quad P is deactivated (activated) for FPGA 1 (2), and that Quad G is deactivated (activated) for FPGA 2 (1). This is defined in the YAML configuration files for FPGA [1](https://gitlab.cern.ch/cms-tracker-phase2-onlinesw/apollo-herd/-/blob/master/etc/PinInfo_Rev2b_VU13p_F1.yaml?ref_type=heads) and [2](https://gitlab.cern.ch/cms-tracker-phase2-onlinesw/apollo-herd/-/blob/master/etc/PinInfo_Rev2b_VU13p_F2.yaml?ref_type=heads), but it is important to keep in mind when writing configuration files for the link tests.
- **4 Channel XCVR**:
    - These links will be routed to the DTH when the boards are deployed. 
    - They are rated at 25G. 
    - In Rev2b, fireflies 4-7 (corresponding to quads AF, T, U, and V) correspond to these links. This is defined in the YAML configuration files for FPGA [1](https://gitlab.cern.ch/cms-tracker-phase2-onlinesw/apollo-herd/-/blob/master/etc/PinInfo_Rev2b_VU13p_F1.yaml?ref_type=heads) and [2](https://gitlab.cern.ch/cms-tracker-phase2-onlinesw/apollo-herd/-/blob/master/etc/PinInfo_Rev2b_VU13p_F2.yaml?ref_type=heads).
- **12 Channel SAMTEC**:
    - These links will be routed to the DTH when the boards are deployed. 
    - They are rated at 25G. 
    - Not sure of the exact position, but one of FF1-3 on each FPGA will correspond to these links. This is defined in the YAML configuration files for FPGA [1](https://gitlab.cern.ch/cms-tracker-phase2-onlinesw/apollo-herd/-/blob/master/etc/PinInfo_Rev2b_VU13p_F1.yaml?ref_type=heads) and [2](https://gitlab.cern.ch/cms-tracker-phase2-onlinesw/apollo-herd/-/blob/master/etc/PinInfo_Rev2b_VU13p_F2.yaml?ref_type=heads).
    - Please note that at the time of writing, SAMTEC has recalled these links so they are not currently deployed on any board.
- **12 Channel CERN B**:
    - These links will be routed to the Frontend when the boards are deployed. 
    - They are rated at 10G. 
    - Not sure of the exact position, but two of FF1-3 on each FPGA will correspond to these links. This is defined in the YAML configuration files for FPGA [1](https://gitlab.cern.ch/cms-tracker-phase2-onlinesw/apollo-herd/-/blob/master/etc/PinInfo_Rev2b_VU13p_F1.yaml?ref_type=heads) and [2](https://gitlab.cern.ch/cms-tracker-phase2-onlinesw/apollo-herd/-/blob/master/etc/PinInfo_Rev2b_VU13p_F2.yaml?ref_type=heads).

## Optical Link Connection Types
- **Loopback**
    - In Herd, you can only do a loopback test with a physical loopback connector plugged into the desired firefly
    - In EMP Butler, you can do a loopback test with this same physical loopback connector, or with the argument **--loopback=nearPMA**. However, be careful as this nearPMA argument only tests the FW internal to the FPGA, and never passes anything through the physical link.
- **12 Channel Connectors**
    - When connecting two 12-channel connectors, please understand which kind of connector you are using based off this [firefly polarity sheet](https://img-en.fs.com/file/white_paper/understanding-fiber-polarity.pdf). This will determine the channel mapping between the optical links.

## Bitstream
- **CMS Standard Trigger Link Protocol (CSP)**:
    - Used to transmit LHC-synchronous data over asynchronous links between backend boards
    - For 25G (16G) links, it has a clock rate of 360 (240) MHz
    - More details can be found [here](https://serenity.web.cern.ch/serenity/emp-fwk/firmware/backend-links.html)
- **Pseudo-Random Binary Sequence (PRBS)**:
    - A statistically random sequence of 1s and 0s that is used as a 'worst case scenario' for a link at its rated speed
    - Useful for identifying bit errors, link instability, or other transmission issues under realistic operating conditions.
    - Note that PRBS does not pass data to the buffers! So during an EMP butler test using PRBS, nothing sensical will be read out from the Rx buffers after the test.

## Clocking
- **Recovered**:
    - A clock signal derived directly from the data stream, ensuring synchronization with the incoming data at the receiving end.
- **Redistributed**:
    - A regenerated clock signal distributed across the system to maintain timing synchronization among different components.

# Example Scripts/Configurations
- An example [input_data.txt](https://gitlab.cern.ch/cms-tracker-phase2-onlinesw/apollo-herd/-/blob/master/FW_Link_Tests/input_data.txt?ref_type=heads) file with incremental numbering for easy debugging

- An example [NullAlgorithm.yaml](https://gitlab.cern.ch/cms-tracker-phase2-onlinesw/apollo-herd/-/blob/master/FW_Link_Tests/NullAlgorithm.yaml?ref_type=heads) configuration file for doing algorithm tests with a null payload.

- An example [Copper_LinkTest_TCDS.yaml](https://gitlab.cern.ch/cms-tracker-phase2-onlinesw/apollo-herd/-/blob/master/FW_Link_Tests/Copper_LinkTest_TCDS.yaml?ref_type=heads) configuration file for doing a link test between the copper links using TCDS clock reset and CSP.

- An example [Copper_LinkTest_TCDS_Mask.yaml](https://gitlab.cern.ch/cms-tracker-phase2-onlinesw/apollo-herd/-/blob/master/FW_Link_Tests/Copper_LinkTest_TCDS_Mask.yaml?ref_type=heads) configuration file for doing a link test between the copper links using TCDS clock reset and CSP while masking some random links as an example.

- An example [XCVR_LinkTest_TCDS.yaml](https://gitlab.cern.ch/cms-tracker-phase2-onlinesw/apollo-herd/-/blob/master/FW_Link_Tests/XCVR_LinkTest_TCDS.yaml?ref_type=heads) configuration file for doing a link test between the bi-directional 4-Channel links using TCDS clock reset and CSP. The stream is transmitted from FF6 and received on FF7 on both FPGAs 1 and 2.

- An example [CernB_EMPButler_FF2_TCDS.yaml](https://gitlab.cern.ch/cms-tracker-phase2-onlinesw/apollo-herd/-/blob/master/FW_Link_Tests/CernB_EMPButler_FF2_TCDS.yaml?ref_type=heads) configuration file for doing a link test in EMP Butler between the 12-channel links using TCDS clock reset and CSP. The stream is transmitted from FF2 on FPGA 1 to FPGA 2.

- An example [Copper_EMPButler_PRBS_Loopback.yaml](https://gitlab.cern.ch/cms-tracker-phase2-onlinesw/apollo-herd/-/blob/master/FW_Link_Tests/Copper_EMPButler_PRBS_Loopback.yaml?ref_type=heads) configuration file for doing a Loopback link test in EMP Butler using the copper links using TCDS clock reset and PRBS.