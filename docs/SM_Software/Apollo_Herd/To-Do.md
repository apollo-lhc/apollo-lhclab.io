# To-Do List
1. The revision of the board is currently defined as a global variable in the [entrypoint_env.sh file](https://gitlab.cern.ch/cms-tracker-phase2-onlinesw/apollo-herd/-/blob/master/ci/entrypoint_env.sh?ref_type=heads). It would be better if there was a register we could read for this.

2. The slot number and assigned crate needs to be read from a register to be used when this is deployed on a larger-scale. The idea would be able to access this info in the CMFPGA class and have it registered to EMP (how Serenity does it).

3. New Cornell MCU FW will have per-channel monitoring information for the FFs. This has to be implemented into the individual FireflyRx/Tx/Bi/Uni classes. These classes only have the per FF monitoring information, not per FF and per channel as the latter registers are yet to be available.

### Resolved
1. Addressed PowerUp/PowerDown bug described in Known Issues subsection in Set-up section. Link to Git [issue](https://gitlab.cern.ch/cms-tracker-phase2-onlinesw/apollo-herd/-/issues/1) 

2. Kernel panic requiring cold reset with IPMC occured when conducting back to back linktests on same herd image. This stopped happening when Vivado and PetaLinux tools where [updated](https://gitlab.com/apollo-lhc/FW/SM_ZYNQ_FW/-/tree/update_v2023.2_jonni?ref_type=heads) (2020.2 to 2023.2). [Registers reads](https://gitlab.cern.ch/cms-tracker-phase2-onlinesw/apollo-herd/-/blob/master/src/common/commands/Program.cpp?ref_type=heads#L87-185) done to check if the command module was correctly programmed were responsible for the kernel panic, but this is seemingly no longer an issue.

3. Formatting the tarball for the EMP+CM_FPGA FW in a way that is compatible with EMP Herd but lets us both load FWs in a single tarball.

4. Implementing UIO daemon into Apollo Herd to not have to directly mount the local /fw/* directories.

5. Alma8 implementation for the Centos7 images so that the OS matches between image and blade. Tom said this is not necessary but may be nice for completeness.

