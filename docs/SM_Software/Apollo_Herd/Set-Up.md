# Setting Up Herd
Follow the steps below to bring up a Herd instance:

1. Clone the [Apollo Herd Plugin](https://gitlab.cern.ch/cms-tracker-phase2-onlinesw/apollo-herd) repo onto your board - the location on the blade is not really important. 

2. Depending on the branch you want to use, checkout that branch, or if developing a new feature create a new branch with the format 'feature/XYZ'. 
    - Generally do not push directly to master, instead develop in a separate branch and create a MR to update master.

3. Ensure that the SM Filesystem and Cornell MCU address tables are up on the blade. 
    - The SM FS is needed to verify C2C AXI/AXILite links are set-up appropriately. 
    - The Cornell MCU is needed to access firefly monitoring information (currently only "IsPresent" and "Optical Power"). 
        - The instructions for the installation of the Cornell MCU FW can be found [here](https://apollo-lhc.gitlab.io/CM_FW/CM_MCU/Installation/).

4. Find the specific docker image associated to the branch you are working with. 
    - Access the available [alma8 images](https://gitlab.cern.ch/cms-tracker-phase2-onlinesw/apollo-herd/container_registry/20616) in the Apollo Herd repo. We have moved to alma8 from centos7 due the deprecation of the latter and to match the OS on our boards.

5. To just start the image without entering it interactively, use [./start_apolloherd.sh](https://gitlab.cern.ch/cms-tracker-phase2-onlinesw/apollo-herd/-/blob/master/start_apolloherd.sh?ref_type=heads). This effectively runs a large Docker command setting up the image.

    ```
    ./path/to/start_apolloherd.sh -e APOLLO_TYPE=Rev2b -d -p 3000:3000 gitlab-registry.cern.ch/cms-tracker-phase2-onlinesw/apollo-herd/alma8-arm64/herd:YourTagHere
    ```

6. If you instead want to instead enter the image interactively (for debugging other reasons), you can run the command below. It is effectively the same as what the  [./start_apolloherd.sh](https://gitlab.cern.ch/cms-tracker-phase2-onlinesw/apollo-herd/-/blob/master/start_apolloherd.sh?ref_type=heads) does, but the **-it** part allows you to enter the image interactively instead of just launching it on the blade.
    - You can also run the Herd instance using GDB for advanced debugging by setting the GDB global variable inside entrypoint.sh to True/.

    ```
    docker run -it --privileged --net=host -v /dev:/dev -v /opt/address_table:/opt/address_table -v /path/to/your/apollo-herd/etc/:/etc/Apollo/ -v /proc:/proc -v /fw/:/fw/ -v /tmp/:/tmp/ --entrypoint bash -e APOLLO_TYPE=YOUR_REV -e CONFIG_EXT=YOUR_EXT gitlab-registry.cern.ch/cms-tracker-phase2-onlinesw/apollo-herd/alma8-arm64/herd:YourTagHere

    bash entrypoint.sh herd.yml
    ```

7. 
    - To verify your herd instance is running when launching herd with the ./start_apolloherd.sh bash script, write 'docker ps' in your command-line and the image should be present. 
    - If entering the image interactively, then after running 'bash entrypint.sh herd.yml' there should be information being outputted to the terminal. If you get an error message and/or recover access to the command-line, this means the instance is down.

This page was last updated - 1/17/2025

# Setting Up Shep
Shep is brought up on a machine that is separate to the one hosting the Apollo Herd docker image instance. At Boston University this is the the "server-room" machine accessed via Tesla, while at the TIF it is the "Aethon" machine accessed from Lxplus. Note that the TIF Shep instance is shared with Serenity, make sure to consult with Tom Williams before making any changes to the set-up there so no work is lost.

1. The repository for Shep is found [here](https://gitlab.cern.ch/cms-tracker-phase2-onlinesw/deployment-and-documentation/-/tree/v0.4-shepherd) with the most recent version as of the time of writing this v0.4. 

2. Clone this repo somewhere on your machine.

3. Due to latency issue between NA and EU, the length of the 'lease' required to run some Shep commands was doubled by Tom. Modify the image url for the shep-ui container in [podman-compose.yml](https://gitlab.cern.ch/cms-tracker-phase2-onlinesw/deployment-and-documentation/-/blob/v0.4-shepherd/deployments/docker-compose/podman-compose.yml) or [docker-compose.yml](https://gitlab.cern.ch/cms-tracker-phase2-onlinesw/deployment-and-documentation/-/blob/v0.4-shepherd/deployments/docker-compose/docker-compose.yml) as below:

    ```
    # image: gitlab-registry.cern.ch/cms-tracker-phase2-onlinesw/shep/shep-ui:v0.4.0 # OLD IMAGE
    image: gitlab-registry.cern.ch/cms-tracker-phase2-onlinesw/shep/shep-ui:longer-lease-4f04a03d
    ```

4. If running on server-room an important change must be made to access the IP addresses corresponding to the BU blades. In [nginx.conf](https://gitlab.cern.ch/cms-tracker-phase2-onlinesw/deployment-and-documentation/-/blob/v0.4-shepherd/deployments/docker-compose/nginx.conf), make the change below:

    ```
    #resolver 127.0.0.11 ipv6=off valid=40s;  # OLD AND REPLACED WITH THE LINE BELOW
    resolver 192.168.255.1 ipv6=off valid=40s;  # The DNS IP server
    ```

5. To bring up Shep run the command below. It should give exit code 0 unless something went wrong bringing up the containers.

    ```
    podman-compose -f podman-compose.yml up -d
    ```

6. To bring the containers down for a reset or other change, you can run the following command:

    ```
    podman-compose -f podman-compose.yml down
    ```

Note: if default ports (3000, etc.) are taken, you can change them in `podman-compose.yml` file and adapt when running the Shep UI.

# Linking Shep and Herd
Given that a Herd instance is running on your blade, and the Shep images are up on your machine, it is time to access the Shep GUI: 

1. Download Firefox onto your local computer

2. Enter the Firefox settings and go to 'Network Settings'. Here, switch to a manual proxy configuration and change the SOCKS Host to 'localhost' with Port 9999. Make sure that SOCKS v5 is selected, and that the checkbox for 'Proxy DNS when using SOCKS v5' is selected as well!

3. Use port-forwarding to see the Shep GUI from the remote machine on our local Firefox instance. Run the command below:

    ```
    ssh -J user@gateway (tesla or lxplus) -D 9999 user@machine (server-room or aethon)
    ```

4. Start Firefox on your local machine and type `localhost:3000` or `http://0.0.0.0:3000/` in the browser (or a different port if configured otherwise). 
    - This should take you to the Shep screen where you can see the status or register any available boards.
    
5. If you have not registered any boards, then you can press the register button to start the process. 
    - Registering the Crate is not necessary. 
    - Click the Board tab, enter the ID for the board you are registering (name you want to give it).
    - Enter the fully qualified IP address of the board for the hostname. As an example:

    ```
    apollo211-1.edf.lo - Extension of .edf.lo for boards at BU 
    cmsapollo213.cern.ch - Extension of .cern.ch for boards at the TIF
    ```

6. If the connection between the Herd instance on your board and the Shep instance on the machine is successful, a green checkmark should appear on the board status screen next to your board. 
    - If the connection is not successful, use the 'curl' command from both your board and machine. This will prints a YAML file to your screen describing the connection on the specified port.

    ```
    curl localhost:3000 - From the board
    curl Board-Hostname:3000 - From your machine
    ```

## Known Issues
If working on machines with limited space, it is important to "clean-up" after working on a board. Specifically, running a herd image takes up ~1.1 GB of space on the machine. The clean-up is simple, bring down your board using "docker kill IMAGE_ID" (or exit if in the image interactively) and then run the following command:

```
    podman system prune -a
```
