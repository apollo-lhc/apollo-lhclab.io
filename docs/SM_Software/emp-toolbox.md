# emp-toolbox

The [emp-toolbox](https://gitlab.cern.ch/p2-xware/software/emp-toolbox/-/tree/master?ref_type=heads) controls and monitors the EMP firmware framework that is used on CM FPGAs of our blade. These libraries do not come as default with our OS and have to be built on the board directly until the RPM install is adapted to our platform. This page contains some hacks needed to build and some instructions on usage of this software.

## Build

* Follow [building on the board](https://gitlab.cern.ch/p2-xware/software/emp-toolbox/-/tree/master?ref_type=heads) instructions up until invoking `make`

* Change to `python3.6` for the `Centos 8` case (release that we currently use) in the `Makefile` of [the python build](https://gitlab.cern.ch/p2-xware/software/emp-toolbox/-/blob/master/python/Makefile?ref_type=heads#L3) 

* Do the same for the `/script/env.sh` [file](https://gitlab.cern.ch/p2-xware/software/emp-toolbox/-/blob/master/scripts/env.sh?ref_type=heads)

* Build the package with `make`

* If the package-install checks from `/script/env.sh` fail, and you already installed these dependecies, comment them out

```
chkpypkg uhal
chkpypkg click
chkpypkg click_didyoumean
```


## Basic usage

These CLI tools are useful when developing to test the FW functionality. One can do the register read with:

```empbutler -c /opt/address_table/emp_connections.xml do F1_IPBUS inspect path.to.node.register```

where `/opt/address_table/emp_connections.xml ` is a connection file created by `uio-daeomn` after programming an FPGA, `F1_IPBUS` is a device name (IPBUS uio, which used for transactions). More instructions can be found on emp-fwk documents.
