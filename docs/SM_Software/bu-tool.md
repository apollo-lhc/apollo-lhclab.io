# BUTool
The BUTool software documentation page can be found [here](https://bu-edf.gitlab.io/BUTool/BUToolDocs).

## Plugin loading
Since BUTool doesn't want to have every type of hardware it can talk to compiled in, plugins are used to allow runtime loading of different hw device libraries. 
### Manual loading ###
Manual loading of plugins in BUTool is accomplished by either using the *add_lib* command in the tool or specifying a plugin .so file on the command line with the "-l" option.
```
  -l [ --lib ] arg          Device library plugin to load
```
### Environment Variables ###
On execution, BUTool checks the environment variable __$BUTOOL_AUTOLOAD_LIBRARY_LIST__ for a list of .so library files for plugins to be loaded.   
This method is ignored if the config file is used.  
Plugin libraries can be loaded multiple times without error. 
```
[cms@apollo06 ~]$ echo $BUTOOL_AUTOLOAD_LIBRARY_LIST 
:/opt/BUTool/lib/libBUTool_GenericIPBusDevice.so :/opt/BUTool/lib/libBUTool_GenericIPBusDevice.so:/opt/BUTool/lib/libBUTool_ApolloSMDevice.so :/opt/BUTool/lib/libBUTool_GenericIPBusDevice.so:/opt/BUTool/lib/libBUTool_ApolloSMDevice.so:/opt/BUTool/lib/libBUTool_IPMISOLDevice.so
```
### Config file ###
BUTool checks the system for a /etc/BUTool config file. 

Ex:

```
[root@apollo06 ~]# cat /etc/BUTool
#For BUTool.cxx
DEFAULT_ARGS=ApolloSM /opt/address_table/connections.xml
lib=/opt/BUTool/lib/libBUTool_ApolloSMDevice.so
lib=/opt/BUTool/lib/libBUTool_GenericIPBusDevice.so
#ApolloSM=/opt/address_table/connections.xml
```

In the above example, the libraries __/opt/BUTool/lib/libBUTool_ApolloSMDevice.so__ and __/opt/BUTool/lib/libBUTool_GenericIPBusDevice.so__ are automatically loaded with an internal call to *add_lib*. 
For the *ApolloSM* plugin, there are default arguments added so that a call of "BUTool.exe -a" will use those arguments.
A call of BUTool.exe -a /path/to/another/connection_file.xml will use the specified connection file instead of the default.


 
