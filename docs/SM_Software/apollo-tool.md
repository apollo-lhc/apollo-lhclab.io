# ApolloTool

The [ApolloTool meta repository](https://github.com/apollo-lhc/ApolloTool) simplifies building the BUTool & associated plugin software for the Apollo SM Petalinux system via cross compiling.

ApolloTool contains [BUTool](https://github.com/BU-Tools/BUTool/tree/develop) and its plugins [ApolloSM_plugin](https://github.com/apollo-lhc/ApolloSM_plugin/tree/develop), [BUTool-IPBUS-Helpers](https://github.com/BU-Tools/BUTool-IPBUS-Helpers/tree/develop) and [GenericIPBus_plugin](https://github.com/BU-Tools/GenericIPBus_plugin/tree/5dc0ec5425e7940115980d3d3837b1afd411a094) as submodules. 

## Dependencies
* [uHAL](https://ipbus.web.cern.ch/doc/user/html/software/installation.html) (with plugin support)
* [UIOuHAL](https://github.com/BU-Tools/UIOuHAL)
* BUTool's dependencies 
* The ApolloSM_plugin and GenericIPBus_plugin both depend on BUTool-IPBUS-Helpers (all included as submodules)

## Build Instructions
Checkout the ApolloTool repository, then update all the submodules with

```bash
make init
```

You can then build the software with 

```bash
make local RUNTIME_LDPATH=/path/to/BUTool COMPILETIME_ROOT=--sysroot=/
```

After building, you can install the software to the desired location with 

```bash
make install RUNTIME_LDPATH=/path/to/BUTool COMPILETIME_ROOT=--sysroot=/ INSTALL_PATH=/path/to/install/location
```

**Note:** You can build and install the software in one command with the above command. 

## Build Variables
* The variable `CACTUS_ROOT` or `IPBUS_PATH` must be set either in the Makefile or on the command line to point to where uHAL was installed. On the Apollo Service Module, the location is `CACTUS_ROOT=/opt/cactus`

* The `RUNTIME_LDPATH` and `COMPILETIME_ROOT` variables are set on the command line and are passed on to the plugins. 

* By default, ApolloTool expects to be building against uHAL version `2.7.x`. If the version of ipbus-software is greater (version `2.8.x`), then the variables `UHAL_VER_MAJOR=2` and `UHAL_VER_MINOR=8` must be specified. These variables will be passed on to the plugins. 

## Debugging & Development
The Apollo [SM_ZYNQ_FW](https://github.com/apollo-lhc/SM_ZYNQ_FW) repository handles building the Zynq FPGA firmware as well as the SoC's Linux kernel and OS. By default, it uses the ApolloTool repository to install BUTool (with the above mentioned plugins) to `/opt/BUTool`. In addition, the OS is built with a default BUTool config file at `/etc/BUTool` that loads the ApolloSMDevice and GenericIPBusDevice classes and starts BUTool with an ApolloSM class.

If you are developing a new plugin for BUTool or updating an existing one, you will want to ensure that you are using the BUTool and plugin libraries you have been developing, *not* the ones installed on the OS already. 

To make sure that you're using the proper libraries, you must:

* comment out all the lines in `/etc/BUTool` using `#`
* unset the `BUTOOL_AUTOLOAD_LIBRARY_LIST` environment variable
* point the `LD_LIBRARY_PATH` variable to the `lib` directories of your copy of `ApolloTool/plugins/*/lib` (and `BUTool/lib`)

An ApolloTool development workflow might then look something like:

```bash
$ git clone https://github.com/apollo-lhc/ApolloTool.git
$ cd ApolloTool/ && make init 
$ make install RUNTIME_LDPATH=/your/path/to/BUTool COMPILETIME_ROOT=--sysroot=/ INSTALL_PATH=/your/path/to/BUTool
```

After installation, head to where you installed BUTool, clear all variables, and launch the executable:

```bash
$ cd /your/path/to/BUTool
$ unset BUTOOL_AUTOLOAD_LIBRARY_LIST
$ LD_LIBRARY_PATH=/your/path/to/ApolloTool/plugins/<plugin_name>/lib/:/your/path/to/BUTool/lib/
$ ./bin/BUTool.exe
```

Following this workflow ensures that you are using your own libraries during BUTool & plugin development. 