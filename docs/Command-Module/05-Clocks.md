# Clocks

This short guide is aimed to show user the required steps for configuring internal/external clocks and TCDS2 signal on the board. 

## Clocks and links configuration with the EMP SW

After programming the both CM FPGAs, user can perform the reset and configuration of clocks and optical/inter-FPGA links. These procedures are usually handled by the online SW [apollo-herd](https://apollo-lhc.gitlab.io/SM_Software/ApolloHerd/Introduction/), but the following command line tools can be used for debugging:

Reset the TTC block

```bash 
empbutler -c /opt/address_table/emp_connections.xml do F1_IPBUS reset tcds2
empbutler -c /opt/address_table/emp_connections.xml do F2_IPBUS reset tcds2
```
With `/opt/address_table/emp_connections.xml` being the emp connection file and `F1_IPBUS`, `F2_IPBUS` the devices IDs specified there.

The expected output is following:
<details>
  <summary>Click here to expand/collapse</summary>

    ```
    17-06-24 08:42:45.556294 [281473189008416] NOTICE - mmap client with URI "ipbusmmap-2.0:///dev/uio_F1_IPBUS?offset=0x0" : Address offset set to 0x0
    Resetting device 'F1_IPBUS'
    Changing clock and TTC source to: TCDS2
    Clock 40 locked after 0 ms
    TTC BC0 locked
    Global BC0 locked

    STATUS
    Clock source: TCDS2
        TTC source: TCDS2
    Has TCDS2 Relay: True
        TTC2 Delay: 0
    Internal BC0: False

    Clock locked: True
        Frequency: 40.079 MHz

        BC0 locked: True
        Dist locked: True

        Bunch count: 0x000805
        Orbit count: 0x001608
        Event count: 0x000000
        BC0s recvd: 5641 valid, 0 missing, 0 invalid

    External interface (rx, tx):
            Mode: Normal
        Power good: True
        PLL lock: True, True
        Ref clock: 320.629 MHz
        Reset done: True, True
            Ready: True, True
        Frame lock: True
    Unlock count: 0

    ```
</details>

Configure MGTs specifying the cahnnels list with `-c` option

```
[root@apollo212-1 ~]# empbutler -c /opt/address_table/emp_connections.xml do F1_IPBUS mgts configure tx -c 4-15,20-59
17-06-24 15:07:26.824285 [281473072706592] NOTICE - mmap client with URI "ipbusmmap-2.0:///dev/uio_F1_IPBUS?offset=0x0" : Address offset set to 0x0
Firmware revision - design: 0x13 infra: 0x000900 algo: 0xcafebeef
Channels - [0-127]  
Powering up QPLL and Tx links in regions [1-3,5-14]
Configuring channels [4-15,20-59]
Resetting links in regions [1-3,5-14]
Resetting latched + counters

Running post-configure status checks ...
 ... all channels report good status!
```

Finally, measure clocks on MGTs

```
empbutler -c /opt/address_table/emp_connections.xml do F1_IPBUS mgts measure-clocks -c 1-3,5-14
```

<details>
  <summary>Click here to expand/collapse</summary>

    ```
    17-06-24 09:00:31.667560 [281472852198432] NOTICE - mmap client with URI "ipbusmmap-2.0:///dev/uio_F2_IPBUS?offset=0x0" : Address offset set to 0x0
    Firmware revision - design: 0x12 infra: 0x000900 algo: 0xcafebeef
    Channels - [0-127]  
    Measuring ref clocks
    -> Channel [04]
        Async ref : 322.264 MHz
        Sync ref  : Unused
        Rx        : 405.682 MHz
        Tx        : 405.682 MHz
    -> Channel [05]
        Async ref : 322.264 MHz
        Sync ref  : Unused
        Rx        : 405.682 MHz
        Tx        : 405.682 MHz
    -> Channel [06]
        Async ref : 322.264 MHz
        Sync ref  : Unused
        Rx        : 405.682 MHz
        Tx        : 405.682 MHz

     .........................
    ```
</details>

If errors observed at any of these stages, the following steps might help to identify and solve the clock / TTC related problems.

## SM Clock TCDS2 path

To make the clock path selection in the SM for TCDS2, do the following:

```
BUTool -a 
>write SERV.CLOCKING.HQ_SEL 0
>write SERV.TCDS.TTC_SOURCE 0
```

this sets the path of the clocks from the backplane to the CM.

## SM Clock Synthesiser

If the SM clock synthesiser is not programmed, the TCDS2/LHC-clock is not passed to the CM. Check SM registers to verify this:

```
BUTool.exe -a

>status 9 CLOCKING
```

LHC, TCDS and HQ frequencies have to be the following:

```
                    CLOCKING| EN|   FREQ| IBUF_EN| OE| SOURCE| TCDS|
                          --|---|-------|--------|---|-------|-----|
                         AXI|   |  50.00|        |   |       |     |
                          EN|   |       |        |   |       |    1|
 F1_C2C_INTF C2C_1 USER_FREQ|   |   0.78|        |   |       |     |
 F1_C2C_INTF C2C_2 USER_FREQ|   |   0.78|        |   |       |     |
 F2_C2C_INTF C2C_1 USER_FREQ|   |   0.78|        |   |       |     |
 F2_C2C_INTF C2C_2 USER_FREQ|   |   0.78|        |   |       |     |
                          HQ|   | 320.63|       1|   |      0|     |
                         LHC|   |  40.08|       1|   |      1|     |
                          OE|   |       |        |   |       |    1|
                      SI5344|  1|       |        |  1|       |     |
                        TCDS|   | 320.63|        |   |       |     |
                         TTC|   |       |       0|   |       |     |
```

otherwise, follow this [instruction](https://apollo-lhc.gitlab.io/SM_Firmware/10-SM_clock_synthesiser/) to program the chip.
## CM Clock Synthesisers
The clocking infrastructure and schematics is described in [the CM manual](https://apollo-lhc.gitlab.io/cornell-cm/Manual/Clocks/01-Overview/). Before any manipulations make sure that CM is powered on with `cmpwrup` command.
To read the clock Synthesisers configuration run the following:

```
BUTool -a
>uart_cmd CM_1 clkmon 0
```

Showing the following output for a programmed chip:

```
Recieved:


Monitoring SI clock with id R0A
REG_TABLE       REG_ADDR BIT_MASK  VALUE 
PN_BASE         : 0x0002   0xffff    0x0000
DEVICE_REV      : 0x0005   0x00ff    0x0000
I2C_ADDR        : 0x000b   0x00ff    0x0000
STATUS          : 0x000c   0x0035    0x0000
LOS             : 0x000d   0x0015    0x0000
LOSIN_FLG       : 0x0012   0x000f    0x0000
STICKY_FLG      : 0x0011   0x002f    0x0000

Program (read from clock chip): R0Av0004
Program (read from eeprom): R0Av0004
```

and for a chip without the clock program:

```
Recieved:


Monitoring SI clock with id R0A
REG_TABLE       REG_ADDR BIT_MASK  VALUE 
clkmon: stale data, last update 18 minutes ago
PN_BASE         : 0x0002   0xffff    0x0000
DEVICE_REV      : 0x0005   0x00ff    0x0000
I2C_ADDR        : 0x000b   0x00ff    0x0000
STATUS          : 0x000c   0x0035    0x0000
LOS             : 0x000d   0x0015    0x0000
LOSIN_FLG       : 0x0012   0x000f    0x0000
STICKY_FLG      : 0x0011   0x002f    0x0000

Program (read from clock chip): 
Program (read from eeprom): 
```

Configuration of 5 chips has to be verified and programmed if found to be wrong/absent. These chips are R0A, R0B, R1A, R1B, and R1C (clkmon 0-4) and should have a similar configuration to the one shown below:

<details>
  <summary>Click here to expand/collapse</summary>

    ```
    >uart_cmd CM_1 clkmon 0   
    Recieved:


    Monitoring SI clock with id R0A
    REG_TABLE       REG_ADDR BIT_MASK  VALUE 
    PN_BASE         : 0x0002   0xffff    0x0000
    DEVICE_REV      : 0x0005   0x00ff    0x0000
    I2C_ADDR        : 0x000b   0x00ff    0x0000
    STATUS          : 0x000c   0x0035    0x0000
    LOS             : 0x000d   0x0015    0x0000
    LOSIN_FLG       : 0x0012   0x000f    0x0000
    STICKY_FLG      : 0x0011   0x002f    0x0000

    Program (read from clock chip): R0Av0004
    Program (read from eeprom): R0Av0004


    >uart_cmd CM_1 clkmon 1
    Recieved:


    Monitoring SI clock with id R0B
    REG_TABLE       REG_ADDR BIT_MASK  VALUE 
    PN_BASE         : 0x0002   0xffff    0x5395
    DEVICE_REV      : 0x0005   0x00ff    0x0000
    I2C_ADDR        : 0x000b   0x007f    0x0068
    LOSXAXB         : 0x000c   0x0002    0x0000
    LOSOOF_IN       : 0x000d   0x00ff    0x0000
    LOL             : 0x000e   0x0002    0x0002
    STICKY_FLG      : 0x0011   0x0027    0x0001

    Program (read from clock chip): 5395ABP1 (not found)
    Program (read from eeprom): X


    >uart_cmd CM_1 clkmon 2
    Recieved:


    Monitoring SI clock with id R1A
    REG_TABLE       REG_ADDR BIT_MASK  VALUE 
    PN_BASE         : 0x0002   0xffff    0x5395
    DEVICE_REV      : 0x0005   0x00ff    0x0000
    I2C_ADDR        : 0x000b   0x007f    0x0068
    LOSXAXB         : 0x000c   0x0002    0x0000
    LOSOOF_IN       : 0x000d   0x00ff    0x0008
    LOL             : 0x000e   0x0002    0x0000
    STICKY_FLG      : 0x0011   0x0027    0x0000

    Program (read from clock chip): R1Av0005
    Program (read from eeprom): R1Av0005


    >uart_cmd CM_1 clkmon 3
    Recieved:


    Monitoring SI clock with id R1B
    REG_TABLE       REG_ADDR BIT_MASK  VALUE 
    PN_BASE         : 0x0002   0xffff    0x5395
    DEVICE_REV      : 0x0005   0x00ff    0x0000
    I2C_ADDR        : 0x000b   0x007f    0x0068
    LOSXAXB         : 0x000c   0x0002    0x0000
    LOSOOF_IN       : 0x000d   0x00ff    0x0022
    LOL             : 0x000e   0x0002    0x0002
    STICKY_FLG      : 0x0011   0x0027    0x0000

    Program (read from clock chip): R1Bv0006
    Program (read from eeprom): R1Bv0006


    >uart_cmd CM_1 clkmon 4
    Recieved:


    Monitoring SI clock with id R1C
    REG_TABLE       REG_ADDR BIT_MASK  VALUE 
    PN_BASE         : 0x0002   0xffff    0x5395
    DEVICE_REV      : 0x0005   0x00ff    0x0000
    I2C_ADDR        : 0x000b   0x007f    0x0068
    LOSXAXB         : 0x000c   0x0002    0x0000
    LOSOOF_IN       : 0x000d   0x00ff    0x0008
    LOL             : 0x000e   0x0002    0x0002
    STICKY_FLG      : 0x0011   0x0027    0x0000

    Program (read from clock chip): 
    Program (read from eeprom): 
    ```
</details>


Follow [the CM manual page](https://apollo-lhc.gitlab.io/cornell-cm/Manual/Clocks/03-loading/) to access the detailed instructions and repos. After checking out [the mcu_tools](https://github.com/apollo-lhc/mcu_tools) and [CM_rev2_HW](https://github.com/apollo-lhc/Cornell_CM_Rev2_HW/tree/master/ClockBuilderPRO) repos, one can use a simple bash script to program all needed clocks. Note that in the example below, 3 clocks (R0A, R1A and R1B), which are needed for the TCDS2 clock distribution to the intra-FPGA links, are being programmed. Contact Cornell group if in doubts about the configuration.

```
#!/bin/bash
python3 mcu_tools/python/LoadClkConfig_tripletEEPROM.py R0A Cornell_CM_Rev2_HW/ClockBuilderPRO/projects/Si5341-RevD-R0Av0004-Registers.txt --tty ttyUL1
python3 mcu_tools/python/LoadClkConfig_tripletEEPROM.py R1A Cornell_CM_Rev2_HW/ClockBuilderPRO/projects/Si5395-RevA-R1Av0005-Registers.txt --tty ttyUL1
python3 mcu_tools/python/LoadClkConfig_tripletEEPROM.py R1B Cornell_CM_Rev2_HW/ClockBuilderPRO/projects/Si5395-RevA-R1Bv0006-Registers.txt --tty ttyUL1
```
In case when clock programs were not updated after loading the clock configs into EEPROM, reset CM MCU, which should load them into chips on boot:

```
BUTool.exe -a
>write CM.CM_1.CTRL.ENABLE_UC 0
>write CM.CM_1.CTRL.ENABLE_UC 1
```


## DTH board

The DAQ and TCDS Hub ([DTH](https://cms-daq-dth-p1.web.cern.ch/dth-kit)) is an ATCA board under development for the CMS Phase-2 upgrade. It is designed for use in the primary hub slot of an ATCA chassis and interfaces sub-system back-end boards in the chassis to central DAQ, TCDS, and networking infrastructure. In this tutorial we are focusing on the Phase-2 Trigger and Timing Control and Distribution (TCDS2) aspects of the DTH. The DTH board is present in the BU and TIF setups and is used to distribute a TCDS2 signal through the crate backplane to the other ATCA boards.

If Apollo does not receive TCDS2 signal, the DTH board most likely needs to be configured to distribute it. Follow [these instructions](https://cmstcds2.docs.cern.ch/latest/pytcds2/) for more details. Short summary is outlined bellow:

- Login to the board @TIF ```DTH@dth-p1-v2-11``` or @BU ```DTH@192.168.30.1```.

- Confirm that the correct firmware version is loaded with ```[DTH@localhost ~]$ tcds2_dth_driver id``` command. The output should be:

```
System ID               : TCDS2
Board ID                : DTH_P1V2
Function ID             : DTH
Firmware version        : 0.1.0
Firmware build timestamp: 20200901T103646
```

- Configure DTH, reload fw, check the id required for tcds2 links

```bash
[DTH@localhost ~]$ tcds2_dth_driver configure
[DTH@localhost ~]$ tcds2_dth_driver reload && sleep 5 && tcds2_dth_driver id
```

output should be:

```
System ID               : TCDS2
Board ID                : DTH_P1V2
Function ID             : DTH
Firmware version        : 0.1.0
Firmware build timestamp: 20200901T103646
```

- Distribute clock & TTC data over the backplane ```[DTH@localhost ~]$ tcds2_dth_driver links init```
