# CM fw build via EMP at BU

## Requests and setup


* Ask `tesla` machine administrator to add your user to the docker group
* Request a membership in `cms-tcds2-users` e-group to access `https://gitlab.cern.ch:8443/cms-tcds/cms-tcds2-firmware.git `. It will take a day or so.
* On tesla machine, `ssh -XY user@tesla.bu.edu`, start a docker container running
 ```bash
 docker run --rm -i -t -v /etc/passwd:/etc/passwd -v ${PWD}/app:/app:Z -e DISPLAY=${DISPLAY} --volume="${HOME}/.Xauthority:/root/.Xauthority:rw" --net=host -v /work/Xilinx/:/work/Xilinx gitlab-registry.cern.ch/cms-cactus/phase2/firmware/docker/alma8/fw-builder:feature-alma8-e71419ba__ipbb2023a
 ```
<details open>
  <summary>Docker basics</summary>
   
  - Hit `exit` to close the container, or press `Ctrl + p`,  `Ctrl + q` to exit without stopping 
  - List running containers with `docker ps` command attach back to your container with  `docker attach <CONTAINER ID>`
  

</details>


* Create and execute the `/app/prepare_setup.sh` script to install several dependencies
```bash
#/bin/bash
set -e
dnf install -y ncurses-compat-libs libXtst libxml2-devel libxslt-devel
pip3 install lxml jinja2

```
* Source the Vivado installation and the license file
``` bash
source /work/Xilinx/Vivado/2020.2/settings64.sh
export XILINXD_LICENSE_FILE=/work/Xilinx/Xilinx.lic
```
## FW build script

*  Create the script defined below to build the EMP fw in `/app/emp_v2p1.sh`. Replace the `username` string with your actual gitlab username. Note that it uses `top_p1_1_lpgbt` example top-file that might change depending on your build.
```bash
#/bin/bash
set -e
gitlab_username="username"
GBT_FPGA_VER="gbt_fpga_6_1_0"
LPGBT_FPGA_VER="v.2.1"
GBTSC_VER="gbt_sc_4_3"
CM_FPGA_FW_VER="v3.0.0"
EMP_FWK_VER="v0.9.0-alpha2"

ipbb init apollo-rev2/
cd apollo-rev2/
ipbb add git https://${gitlab_username}:@gitlab.cern.ch:8443/p2-xware/firmware/emp-fwk.git -b ${EMP_FWK_VER}
ipbb add git https://github.com/apollo-lhc/CM_FPGA_FW -b ${CM_FPGA_FW_VER}
#develop

ipbb add git https://gitlab.cern.ch/ttc/legacy_ttc.git -b v2.1
ipbb add git https://${gitlab_username}:@gitlab.cern.ch:8443/cms-tcds/cms-tcds2-firmware.git -b v0_1_1
ipbb add git https://gitlab.cern.ch/hptd/tclink.git -r fda0bcf
ipbb add git https://github.com/ipbus/ipbus-firmware -b v1.9
ipbb add git https://${gitlab_username}:@gitlab.cern.ch:8443/dth_p1-v2/slinkrocket_ips.git -b v03.12
ipbb add git https://${gitlab_username}:@gitlab.cern.ch:8443/dth_p1-v2/slinkrocket.git -b v03.12
ipbb add git https://gitlab.cern.ch/gbt-fpga/gbt-fpga.git -b ${GBT_FPGA_VER}
ipbb add git https://gitlab.cern.ch/gbt-fpga/lpgbt-fpga.git -b ${LPGBT_FPGA_VER}
ipbb add git https://gitlab.cern.ch/gbtsc-fpga-support/gbt-sc.git -b ${GBTSC_VER}


export PATH=/opt/cactus/bin/uhal/tools:$PATH 
export LD_LIBRARY_PATH=/opt/cactus/lib:${LD_LIBRARY_PATH}

export COLUMNS=120
ipbb proj create vivado vu13p_p1 emp-fwk:projects/examples/apollo/cm_v2/vu13p top_p1_1_lpgbt.dep
cd proj/vu13p_p1/
ipbb ipbus gendecoders
ipbb vivado generate-project --single
ipbb vivado synth -j4 impl -j4
ipbb vivado package
```

 - Comment out the `default_ccache_name = KEYRING:persistent:%{uid}` line in `/etc/krb5.conf` file
 - Run `kinit -f username@CERN.CH`
 - Run the fw build itself `source /app/emp_v2p1.sh`
 - `cd ../../src/CM_FPGA_FW/`
 - `make EMP_Cornell_rev2_p1_VU13p-1-SM_USP.tar.gz`
 or `DTC_FLAGS="" make EMP_Cornell_rev2_p1_VU13p-1-SM_USP.tar.gz` to hide some warnings
 - Replace `p1` with `p2` in the script and `make` target above to make a fw build for FPGA2 .

