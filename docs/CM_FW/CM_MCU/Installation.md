# Installation

This section contains instructions to deploy the CM MCU FW found [here](https://github.com/apollo-lhc/cm_mcu/tree/master). This repository is maintained by Peter Wittich's group at Cornell. 

## Setup

First, locate the latest tag of the FW in the linked repository above. We need two of the assets from the latest tag: ```cm_mcu_rev2.bin``` and ```PL_MEM_CM_rev2.xml```. Get both of these files onto the blade where we want to set up the CM MCU FW. For the .bin file, its location on the blade does not matter. 

However for the .xml file, we need this file to be located in this directory ```/fw/CM/CornellCM_MCU/address_table/modules_CM_MCU```. If this directory does not exist, create it. Additionally, this file must be renamed from ```PL_MEM_CM_rev2.xml``` to ```PL_MEM_CM.xml```.

Now, go to the directory where we have the .bin file and run - ```sflash ./cm_mcu_rev2.bin -c /dev/ttyUL1```

This installs the MCU FW onto the disc and should give a "success" message when installed successfully.

Next, go one directory above where we put our .xml address table - ```/fw/CM/CornellCM_MCU/address_table/```. Here we want to set-up the ```address_apollo.xml``` and ```connections.xml``` files.

## Example - address_apollo.xml

```
<node id="TOP">
  <node id="PL_MEM_CM" address="0x23000000" fwinfo="uio_endpoint" module="file://modules_CM_MCU/PL_MEM_CM.xml" />
</node>
```

## Example - connections.xml

```
<connections>
  <!-- be sure to use the same file in both "uri" and "address_table" -->
  <connection id="test.0" uri="uioaxi-1.0://address_apollo.xml" address_table="uioaxi-1.0://address_apollo.xml" />
</connections>
```

## Example Register Read

In order to verify the registers are now readable and CM MCU FW is set-up correctly, we can attempt a register read from the ```PL_MEM_CM.xml``` address-table.

Open BUTool by typing into the CL - ```BUTool.exe -a```

Test reading a register by typing into the BUTool interface - ```readstring PL_MEM_CM.MCU_FW_VER```

The version output by this read attempt should match the version of the asset tag we originally downloaded from the CM MCU FW github repo.