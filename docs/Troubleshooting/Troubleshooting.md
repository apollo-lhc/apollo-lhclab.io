# Troubleshooting Documentation

The purpose of this page is to document known sequences of commands that result in the failure of some part of the board.

## Programming MCU FW + Resetting UIO Daemon
Steps to reproduce the error include:

1. Install MCU FW and set-up address tables in /fw/CM as described [here](https://apollo-lhc.gitlab.io/CM_FW/CM_MCU/Installation/).
2. Reboot UIO Daemon to find new MCU address tables as described in the second note [here](https://apollo-lhc.gitlab.io/SM_Software/linux-daemons/#uio-daemon).

It brings the Zynq CPU down. A simple reset of the Zynq resolves the issue, and MCU FW registers can be read appropriately. To avoid this issue, simply reboot the board after programming the MCU and setting up the address tables. This will reset the daemon and will use the new address tables. 

## Programming FPGA without Unloading Previous Tar

To load FPGA FW using UIO Daemon, one performs a command that looks like:

```
uio_send.py program-fpga -f YOUR_TARBLL
```

However, if one does not unload the previous tarball prior to programming the FPGA with a new tarball, the board freezes up and the Zynq goes down. To resolve this, reset the Zynq and reprogram with the appropriate tarball.

## Consecutive PowerUp/PowerDown commands in Apollo Shep

The PowerUp and PowerDown commands work when run alone in a fresh instance of Herd. However, when powering down and then quicky powering up the FPGA via Shep, the Herd instance crashes (although the command is successful). The temprorary fix is to restart your herd instance and continue as normal, but this issue is yet to be fully understood and is currently being debugged.
