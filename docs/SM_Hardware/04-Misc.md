# Misc properties

## Set Zynq time

This section describes how to fetch time from an NTP server and update the time in the Zynq SoC.

Requires:

* Battery (CR1220 3V)
* network connection
* /dev/rtc0

**Note:** Please check that RTC battery is properly installed before doing this, you can find the instructions [here](https://apollo-lhc.gitlab.io/Assembly/cm-sm-assembly/#rtc-setup).

You'll need to execute the following commands:

Get time from NTP server, if within BU, one can use the NTP server from BU with the following IP address: `128.197.27.107`
```
ntpdate 128.197.27.107
```
Read the RT clock
```
hwclock -r
```
