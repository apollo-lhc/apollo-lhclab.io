# Interfaces

## Overview

![Top](HW_design/basic/HW_design-Top.drawio.svg)

## UARTs

![UARTs](HW_design/basic/HW_design-UART.drawio.svg)

| Name | source | destination | direction | notes |
| ---  | --- | --- | --- | --- |
| Multi-purpose | FP FTDI |  | bi | muxed, FTDI takes priority over IPMC |
|  | IPMC | | bi |  |
|  |  | Zynq UART1 | bi | Mux addr 0b00 |
|  |  | debug conn J701 | bi | Mux addr 0b01 |
|  |  | CM Mezz 1 (?)| bi | Mux addr 0b01 |
|  |  | CM Mezz 2 (?)| bi | Mux addr 0b01 |
| CM1 MCU console | Zynq | CM MCU | bi | 115200 /dev/ttyUL1|
| CM1 MCU monitor | CM MCU | Zynq PL  | CM->SM | 115200 mapped to PL_MEM_CM.*|
| ESM | Zynq | ESM | bi | 115200 /dev/tty_UL3 |
| C2C1 pBlaze | Zynq | PL | bi | 115200 /dev/ttyUL100 |
| C2C2 pBlaze | Zynq | PL | bi | 115200 /dev/ttyUL101 |
| F1_C2C pBlaze | Zynq | CM PL | bi | 115200 /dev/ttyUL101 (FIX ME!) |
| F1_C2C pBlaze | Zynq | CM PL | bi | 115200 /dev/ttyUL102 (FIX ME!) |


## I2C

![I2C](HW_design/basic/HW_design-Sensor.drawio.svg)



## ZYNQ Network Interfaces

This section describes the ETH network interface of ZYNQ, installed on Apollo Service Module (SM). It will go over the following topics:

* The MAC address formatting for ETH ports
* How to set the MAC addresses through the IPMC terminal
* Obtaining the IP address through the [networking daemon](https://gitlab.com/apollo-lhc/soc-tools/netconfig-daemon)

### MAC Address Format

The MAC address format for the ETH ports are as following:

```
00:50:51:FF:PX:YZ
```

In the above MAC address template:

* P describes the port number. For port ETH0, `P=0` and for ETH1, `P=1`.
* XYZ describes the serial number of the SM in hexadecimal format.

As an example, the `ETH1` port for the ZYNQ on `SM203` service module will have the following MAC address (note that `203` can be translated to `0xCB` in hex):

```
00:50:51:FF:10:CB
```

### Setting the MAC Adress With IPMC

The MAC addresses for the `ETH0` and `ETH1` ports can be set using the IPMC's telnet interface, using the `ethmacwr` command. This will save the MAC address to the EEPROM on the SM, and this information will also be forwarded to ZYNQ's I2C registers. In addition, `ethmacwr` will save an 8-bit checksum of the MAC address being written, which will be used by other network scripts to verify that the MAC address being read is correct (see the below sub-section for details).

The `ethmacwr` command can be used as follows:

```
ethmacwr <port number> <space separated MAC addr>
```

The port number is `0` for `ETH0` port, and `1` for `ETH1` port. Following the earlier example, if we wanted to write the MAC address of the `ETH1` port for the ZYNQ on `SM203`, we can execute the following:

```
ethmacwr 1 00 50 51 FF 10 CB
```

The command will output the EEPROM contents after the write has been done, and we should be able to see that `eth1_mac` field is now properly filled.

### The MAC Address Checksum

For each MAC address on EEPROM, there will be a 8-bit checksum written as well. This checksum will be the two's complement of the sum of MAC address bytes, and will be used to verify the MAC address read from ZYNQ. If EEPROM revision is set to `1` (via `verwr 1`), one can see the 8-bit checksum entries for ETH0 and ETH1 ports in the `eth0_chsum` and `eth1_chsum`, respectively.

## Networking Daemon

Once the MAC address is properly set in EEPROM and ZYNQ's I2C registers, a [networking daemon](https://gitlab.com/apollo-lhc/soc-tools/netconfig-daemon) is responsible (at boot time) to do the following operations:

* Launch the network interface with the MAC address being read
* With the new network interface, contact the DHCP server to obtain an IP address

To see the setup instructions for the networking daemon, one can consult the `README` in the daemon repo [here](https://gitlab.com/apollo-lhc/soc-tools/netconfig-daemon/-/blob/main/README.md). 

### MAC Address: Retrieval

There are a number of possible scenarios for the networking daemon to read the MAC address of the ETH ports. This procedure is described below:

* The daemon will first check ZYNQ's I2C registers for the MAC address. The name of the registers to read can be configured in the `netconfig_daemon.yaml` file.
* If the retrieved MAC address is not valid (or there is a problem doing a `BUTool` read), it will try reading the MAC address from the `/fw/SM/eth0_mac.dat` file (or `eth1_mac.dat` for the ETH1 port).
* If this file doesn't exist, it will fall back into the default case, as described by the configuration file under `/etc/netconfig_daemon`.
* As a last fallback case, if the configuration file is not found under `/etc/netconfig_daemon`, the default MAC address defined in the script will be used.

With the current version of the DHCP server, the service module is expected to get assigned an IP address of `192.168.34.XYZ`, where `XYZ` is the serial number of the service module.

### MAC Address: Validation

The networking daemon will validate the MAC address which is read using `BUTool` (i.e. scenario # 1 in the above section), using the 8-bit checksum values (also read using `BUTool`) for each port. The process is as follows:

* (a) The daemon will compute the 8-bit checksum for the MAC address it obtains
* (b) Then, it will retrieve the two's complement of the 8-bit checksum from ZYNQ's I2C registers
* Finally, the daemon will check if `(a + b) & 0xFF == 0`, i.e. if the two checksums sum up to `0` for the first 8-bits

If this comparison is successful, the MAC address is valid. Otherwise, the daemon will fall to the second scenario, where it will try to read the MAC address from the `.dat` files under `/fw/SM` directory (i.e. scenario # 2 above). 

### Checking Daemon Status

To check that whether the daemon has been running upon boot, the following commands are useful:

```bash
# Get the status of the service
systemctl status netconfig_daemon.service

# Get the logs for this service
journalctl -u netconfig_daemon.service
```