#Boards

## Service Module
This is the primary board for the Apollo platform and hosts the Zynq SoC, Front-panel board, ESM, IPMC, and the Command Module.
The files can be found in the [Kicad Project Repo](https://gitlab.com/apollo-lhc/hardware/apollosm-hw).
#### Important branches
- [__Rev2b in-progress__](https://gitlab.com/apollo-lhc/hardware/apollosm-hw/-/tree/rev2b?ref_type=heads)  
![img](images/SM_rev2b.png){style="height:300px"}  
- [__Rev2a as-built__](https://gitlab.com/apollo-lhc/hardware/apollosm-hw/-/tree/rev2a-as-built?ref_type=tags)  
![img](images/SM_rev2.jpg){style="height:300px"}  
- [__Rev1 as-built__](https://gitlab.com/apollo-lhc/hardware/apollosm-hw/-/tree/rev1-as-built?ref_type=tags)  
![img](images/SM_rev1.jpg){style="height:300px"}  



## Front-panel board
[Kicad Project Repo](https://gitlab.com/apollo-lhc/hardware/apollosm-fp-hw)  
  -  [Rev E](https://gitlab.com/apollo-lhc/hardware/apollosm-fp-hw/-/tree/RevE?ref_type=tags)  
![img](images/FP_revE_top.png){style="height:150px"} ![img](images/FP_revE_bot.png){style="height:150px"}  
  -  [Rev D](https://gitlab.com/apollo-lhc/hardware/apollosm-fp-hw/-/tree/RevD?ref_type=tags)  
![img](images/FP_revD_top.jpg){style="height:150px"} ![img](images/FP_revD_bot.jpg){style="height:150px"}  
-  [Rev C](https://gitlab.com/apollo-lhc/hardware/apollosm-fp-hw/-/tree/RevC?ref_type=tags)  
![img](images/FP_revC_top.jpg){style="height:150px"} ![img](images/FP_revC_bot.jpg){style="height:150px"}  

## IPMC
The Apollo is compatible with the CERN IPMC, UW IPMC, but by default uses the OpenIPMC.
The CERN IPMC and OpenIPMC are plug read, though the UW IPMC requries the installation of power jumpers.  
Links  
  - [Apollo FW Customization](https://gitlab.com/BU-EDF/openipmc-fw)  
  - [Base IPMC FW](https://gitlab.com/BU-EDF/openipmc-fw)  
  - [OpenIPMC Project](https://gitlab.com/openipmc)

## SoC
Enclustra
  - Mercury+ XU8 (US+,rev2) [Link](https://download.enclustra.com/#Mercury+_XU8)  
    ![img]( images/mercury_xu8_front_350.jpg){style="height:200px"} ![img](images/mercury_xu8_back_350.jpg){style="height:200px"}  
  - Mercury ZX1 (7-series,rev1/rev2) [Link](https://download.enclustra.com/#Mercury_ZX1)  
    ![img](images/mercury_zx1_front_600.jpg){style="width:200px"}  ![img](images/mercury_zx1_back_600.jpg){style="width:200px"}   
