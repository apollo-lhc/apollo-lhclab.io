import gitlab
import mkdocs_gen_files
import requests
from datetime import datetime

def file_exists(url):
    """Check if a file exists at the given URL."""
    response = requests.head(url, allow_redirects=True)
    return response.status_code == 200

def FindSMZynqFWTags(remote="https://gitlab.com",
                     project_path="apollo-lhc/FW/SM_ZYNQ_FW",
                     outfileName="SM_Firmware/01-location.md"):

    with mkdocs_gen_files.open(outfileName, "w") as outFile:
        print("# Location:\n",file=outFile)
        print("The SM Zynq FW is located in [gitlab](https://gitlab.com/apollo-lhc/FW/SM_ZYNQ_FW/) and has recently (2023) moved to [Hog](https://cern.ch/hog) for its build system.\n",file=outFile)
        print("## Tags:\n",file=outFile)

    
        #open up the remote connection to the project
        gl = gitlab.Gitlab(remote)
        project = gl.projects.get(project_path)
    
        #get list of tags
        tags=project.tags.list(get_all=True)
        pipelines=project.pipelines.list(get_all=True)
        for tag_obj in tags:
            #get the tag info
            tag_name = tag_obj._attrs['name']
            tag_commit = project.commits.get(tag_obj._attrs['commit']['id'])
            tag_sha    = tag_commit._attrs['id']
            tag_short  = tag_commit._attrs['short_id']
            tag_date   = datetime.strptime(tag_commit._attrs['created_at'], f'%Y-%m-%dT%H:%M:%S.%f%z')
            #set this up to print the banner only once per sha
            printed_banner=False

            for pl in pipelines:              
                if pl._attrs['sha'] == tag_obj._attrs['commit']['id']:
                    pipeline = pl #project.pipelines.get(pipeline_id)
                    pipeline_id=pl._attrs['id']#parent_commit._attrs['last_pipeline']['id']        

                    for job in pipeline.jobs.list():
                        if job._attrs['stage'] == 'user_post':

                            if not printed_banner:
                                print("#### %s" % (tag_name),file=outFile)
                                print(
                                    f"{tag_date.strftime('%Y-%m-%d')}&nbsp;&nbsp;&nbsp;"
                                    f"Commit: [{tag_short}](https://gitlab.com/apollo-lhc/FW/SM_ZYNQ_FW/-/commit/{tag_sha})&nbsp;&nbsp;&nbsp;"
                                    f"[Pipeline](https://gitlab.com/apollo-lhc/FW/SM_ZYNQ_FW/-/pipelines/{pipeline_id})&nbsp;&nbsp;&nbsp;"
                                    f"[Release Notes](https://gitlab.com/apollo-lhc/FW/SM_ZYNQ_FW/-/releases/{tag_name})\n\n",
                                    file=outFile
                                )
                                print("| Build | media | download |", file=outFile)
                                print("|-------|-------|----------|", file=outFile)
                                printed_banner=True
                            
                            name= job._attrs["name"]
                            media_name=""
                            if name.find("sdcard") >= 0:
                                media_name="sdcard"
                                build_name=name[0:name.find("_sdcard")]
                            elif name.find("emmc") >= 0:
                                media_name="emmc"
                                build_name=name[0:name.find("_emmc")]
                            elif name.find("ssd") >= 0:
                                media_name="ssd"
                                build_name=name[0:name.find("_ssd")]
                            build_name= build_name.replace("KERNEL:","").replace("_kernel","")                                
                            build_id  = job._attrs["id"]
                            # Define the two possible URLs
                            url1 = f"https://gitlab.com/apollo-lhc/FW/SM_ZYNQ_FW/-/jobs/{build_id}/artifacts/raw/kernel/zynq_build/{build_name}.{media_name}.docker/{build_name}.tar.gz"
                            url2 = f"https://gitlab.com/apollo-lhc/FW/SM_ZYNQ_FW/-/jobs/{build_id}/artifacts/raw/kernel/zynq_build/{build_name}.{media_name}/{build_name}.tar.gz"

                            # JRF given that we might use docker or native pipelines, check if the files exists at either URL
                            if file_exists(url1):
                                file_link=f"[{build_name}.tar.gz]({url1})"
                            elif file_exists(url2):
                                file_link=f"[{build_name}.tar.gz]({url2})"
                            else:
                                file_link = f"https://gitlab.com/apollo-lhc/FW/SM_ZYNQ_FW/-/jobs/{build_id}"
                                print("WARNING: # (File not found)")
                                print("WARNING: The assets for this tag do not exist or are not present in the pipeline that is associated to this tag.")
                            
                            print(f"| {build_name} | {media_name} | {file_link} ", file=outFile)
            print("\n\n",file=outFile)


def FindSMZynqFSTags(remote="https://gitlab.com",
                     project_path="apollo-lhc/soc-os",
                     outfileName="SM_Filesystem/01-location.md"):

    with mkdocs_gen_files.open(outfileName, "w") as outFile:
        print("# Location:\n",file=outFile)
        print("The SM SoC build is located in [gitlab](https://gitlab.com/apollo-lhc/soc-os/).\n",file=outFile)
        print("## Tags:\n",file=outFile)

    
        #open up the remote connection to the project
        gl = gitlab.Gitlab(remote)
        project = gl.projects.get(project_path)
    
        #get list of tags
        tags=project.tags.list(get_all=True)
        pipelines=project.pipelines.list(get_all=True)
        for tag_obj in tags:
            #get the tag info
            tag_name = tag_obj._attrs['name']
            tag_commit = project.commits.get(tag_obj._attrs['commit']['id'])
            tag_sha    = tag_commit._attrs['id']
            tag_short  = tag_commit._attrs['short_id']
            tag_date   = datetime.strptime(tag_commit._attrs['created_at'], f'%Y-%m-%dT%H:%M:%S.%f%z')

            #set this up to print the banner only once per sha
            printed_banner=False
            
            tag_sha=tag_obj._attrs['commit']['short_id']

            #set this up to print the banner only once per sha
            printed_banner=False

            for pl in pipelines:              
                if pl._attrs['sha'] == tag_obj._attrs['commit']['id']:
                    pipeline = pl #project.pipelines.get(pipeline_id)
                    pipeline_id=pl._attrs['id']#parent_commit._attrs['last_pipeline']['id']                 
                    for job in pipeline.jobs.list():
                        if job._attrs['stage'] == 'build':
                            
                            if not printed_banner:
                                print("#### %s\n" % (tag_name),file=outFile)                                
                                # print("%s  Commit: [%s](https://gitlab.com/apollo-lhc/soc-os/-/commit/%s)  [Pipeline](https://gitlab.com/apollo-lhc/soc-os/-/pipelines/%s)\n\n" % (tag_date.strftime('%Y-%m-%d'),tag_short,tag_sha,pipeline_id), file=outFile)
                                print(
                                    f"{tag_date.strftime('%Y-%m-%d')}&nbsp;&nbsp;&nbsp;"
                                    f"Commit: [{tag_short}](https://gitlab.com/apollo-lhc/soc-os/-/commit/{tag_sha})&nbsp;&nbsp;&nbsp;"
                                    f"[Pipeline](https://gitlab.com/apollo-lhc/soc-os/-/pipelines/{pipeline_id})&nbsp;&nbsp;&nbsp;"
                                    f"[Release Notes](https://gitlab.com/apollo-lhc/soc-os/-/releases/{tag_name})\n\n",
                                    file=outFile
                                )
                                print("| Build | os    | download |", file=outFile)
                                print("|-------|-------|----------|", file=outFile)
                                printed_banner=True

                            name= job._attrs["name"]
                            os_name=""
                            build_name=name
                            if name.find("alma8") >= 0:
                                os_name="alma8"
                                build_name=name[len(os_name)+1:]
                            elif name.find("centos7") >= 0:
                                os_name="centos7"
                                build_name=name[len(os_name)+1:]
                            build_id  = job._attrs["id"]
                            print("| %s | %s | [%s_%s-%s.tar.xz](https://gitlab.com/apollo-lhc/soc-os/-/jobs/%s/artifacts/raw/image/%s_%s-%s.tar.xz) " %(build_name,
                                                                                                                                                         os_name,
                                                                                                                                                         os_name,build_name,tag_sha,
                                                                                                                                                         build_id,
                                                                                                                                                         os_name,build_name,tag_sha),file=outFile)
                    print("\n\n",file=outFile)
                    #since this build doesn't use rebase, we only need the most recent pipeline, so we break here
                    break

        
            
            


FindSMZynqFWTags()
FindSMZynqFSTags()
